<?php
/**
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'extracolumns` (
    `id_extracolumns` INT NOT NULL AUTO_INCREMENT,
    `tab` VARCHAR(63) NOT NULL,
    `replace` BOOLEAN NOT NULL DEFAULT FALSE,
    `start` VARCHAR(63) NOT NULL DEFAULT "action",
    PRIMARY KEY  (`id_extracolumns`),
    UNIQUE (`tab`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'extracolumns_columns` (
    `id_extracolumns_columns` INT NOT NULL AUTO_INCREMENT,
    `id_extracolumns` INT NOT NULL,
    `id_extracolumns_profiles` INT NOT NULL DEFAULT 1,
    `position` INT NOT NULL,
    `table` VARCHAR(63) NOT NULL,
    `attribute` VARCHAR(63) NOT NULL,
    `title` VARCHAR(63) NOT NULL,
    `type` VARCHAR(63) NOT NULL,
    PRIMARY KEY (`id_extracolumns_columns`),
    UNIQUE (`id_extracolumns`, `id_extracolumns_profiles`, `table`, `attribute`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql[] = "DROP TABLE IF EXISTS `" . _DB_PREFIX_ . "extracolumns_profiles`;";
$sql[] = "DROP TABLE IF EXISTS `" . _DB_PREFIX_ . "extracolumns_employee_profile`;";

$sql[] = 'CREATE TABLE `' . _DB_PREFIX_ . 'extracolumns_profiles` (
    `id_extracolumns_profiles` INT NOT NULL AUTO_INCREMENT,
    `id_extracolumns` INT NOT NULL,
    `name` VARCHAR(63) NOT NULL,
    `replace` BOOLEAN NOT NULL DEFAULT FALSE,
    `start` VARCHAR(63) NOT NULL DEFAULT "action",
    PRIMARY KEY (`id_extracolumns_profiles`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE `' . _DB_PREFIX_ . 'extracolumns_employee_profile` (
    `id_employee` INT NOT NULL,
    `id_extracolumns` INT NOT NULL,
    `id_extracolumns_profiles` INT NOT NULL,
    PRIMARY KEY (`id_employee`, `id_extracolumns`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';


$sql[] = "ALTER TABLE `" . _DB_PREFIX_ . "extracolumns_profiles` AUTO_INCREMENT = 2";

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
