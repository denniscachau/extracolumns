<?php
/**
 * 2007-2023 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2023 PrestaShop SA
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

namespace PrestaShop\Module\ExtraColumns\Controller\Admin\Improve\Design;

use PrestaShop\PrestaShop\Core\Grid\Grid;
use PrestaShop\PrestaShop\Core\Grid\GridFactory;
use PrestaShopBundle\Component\CsvResponse;
use PrestaShopBundle\Controller\Admin\FrameworkBundleAdminController;
use PrestaShopBundle\Security\Annotation\ModuleActivated;

/**
 * Class ExtraColumnsController.
 *
 * @ModuleActivated(moduleName="extracolumns", redirectRoute="admin_module_manage")
 */
class ExtraColumnsController extends FrameworkBundleAdminController
{
    public function exportCustomersAction(\PrestaShop\PrestaShop\Core\Search\Filters\CustomerFilters $filters)
    {
        $gridFactory = $this->get('prestashop.core.grid.factory.customer');
        $grid = $gridFactory->getGrid($filters);

        return $this->exportCommon($grid, 'customers');
    }

    public function exportOrdersAction(\PrestaShop\PrestaShop\Core\Search\Filters\OrderFilters $filters)
    {
        $gridFactory = $this->get('prestashop.core.grid.factory.order');
        $grid = $gridFactory->getGrid($filters);

        return $this->exportCommon($grid, 'orders');
    }

    private function exportCommon(Grid $grid, $filename)
    {
        $headers = array();
        foreach ($grid->getDefinition()->getColumns()->toArray() as $column) {
            if (array_key_exists('field', $column['options'])) {
                $headers[$column['id']] = $column['name'];
            }
        }

        $data = array();
        foreach ($grid->getData()->getRecords()->all() as $record) {
            $data_tmp = array();
            foreach ($grid->getDefinition()->getColumns()->toArray() as $column) {
                if (array_key_exists('field', $column['options'])) {
                    $data_tmp[$column['id']] = $record[$column['options']['field']];
                }
            }

            $data[] = $data_tmp;
        }

        return (new CsvResponse())
            ->setData($data)
            ->setHeadersData($headers)
            ->setFileName($filename . '_' . date('Y-m-d_His') . '.csv');
    }
}
