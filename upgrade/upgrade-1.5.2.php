<?php
/**
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_1_5_2()
{
    $sql = array();

    $sql[] = "ALTER TABLE `" . _DB_PREFIX_ . "extracolumns_profiles` 
        ADD `replace` BOOLEAN NOT NULL DEFAULT FALSE AFTER `name`;";

    $increment = (int)Db::getInstance()->getValue(
        "SELECT `AUTO_INCREMENT`
        FROM  `information_schema`.`tables`
        WHERE `table_schema` = 'cachau_blog'
        AND   `table_name` = '" . _DB_PREFIX_ . "extracolumns_profiles'"
    );

    $sql[] = "UPDATE `" . _DB_PREFIX_ . "extracolumns_profiles`
        SET `id_extracolumns_profiles` = `id_extracolumns_profiles` + " . $increment . "
        WHERE `id_extracolumns_profiles` <> 0";

    $sql[] = "UPDATE `" . _DB_PREFIX_ . "extracolumns_profiles`
        SET `id_extracolumns_profiles` = 1
        WHERE `id_extracolumns_profiles` = 0";

    $sql[] = "UPDATE `" . _DB_PREFIX_ . "extracolumns_columns`
        SET `id_extracolumns_profiles` = `id_extracolumns_profiles` + " . $increment . "
        WHERE `id_extracolumns_profiles` <> 0";

    $sql[] = "UPDATE `" . _DB_PREFIX_ . "extracolumns_columns`
        SET `id_extracolumns_profiles` = 1
        WHERE `id_extracolumns_profiles` = 0";

    $sql[] = "UPDATE `" . _DB_PREFIX_ . "extracolumns_employee_profile`
        SET `id_extracolumns_profiles` = `id_extracolumns_profiles` + " . $increment . "
        WHERE `id_extracolumns_profiles` <> 0";

    $sql[] = "UPDATE `" . _DB_PREFIX_ . "extracolumns_employee_profile`
        SET `id_extracolumns_profiles` = 1
        WHERE `id_extracolumns_profiles` = 0";

    foreach ($sql as $query) {
        if (Db::getInstance()->execute($query) === false) {
            return false;
        }
    }

    $auto_increment_value = (int)Db::getInstance()->getValue(
        "SELECT MAX(`id_extracolumns_profiles`)
        FROM `" . _DB_PREFIX_ . "extracolumns_profiles`"
    );
    Db::getInstance()->execute(
        "ALTER TABLE `" . _DB_PREFIX_ . "extracolumns_profiles` AUTO_INCREMENT = " . ($auto_increment_value + 2)
    );

    return true;
}
