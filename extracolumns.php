<?php
/**
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

if (file_exists(_PS_MODULE_DIR_ . 'extracolumns/vendor/autoload.php')) {
    require_once _PS_MODULE_DIR_ . 'extracolumns/vendor/autoload.php';
}

include_once _PS_MODULE_DIR_ . 'extracolumns/classes/ExtraColumnsConfiguration.php';
include_once _PS_MODULE_DIR_ . 'extracolumns/classes/ExtraColumnsLists.php';
include_once _PS_MODULE_DIR_ . 'extracolumns/classes/ExtraColumnsProfile.php';

class Extracolumns extends Module
{
    /**
     * @var bool
     */
    protected $config_form = false;

    /**
     * @var \PrestaShop\PrestaShop\Core\Localization\Locale
     */
    private $locale;

    public function __construct()
    {
        $this->name = 'extracolumns';
        $this->tab = 'administration';
        $this->version = '1.11.1';
        $this->author = 'Ciren';
        $this->need_instance = 1;
        $this->module_key = '46447b7784110a1b634af1058b0d6a03';

        $this->tabs = array(
            array(
                'class_name' => 'AdminLists',
                'visible' => true,
                'name' => $this->l('Lists'),
                'parent_class_name' => 'IMPROVE'
            ),
            array(
                'class_name' => 'AdminExtraColumns',
                'visible' => true,
                'name' => $this->l('Customize columns'),
                'parent_class_name' => 'AdminLists'
            )
        );

        if (version_compare(_PS_VERSION_, '1.7.6.0', '>=')) {
            $this->tabs[] = array(
                'class_name' => 'AdminExtraColumnsGrid',
                'visible' => false,
                'name' => 'Extra columns grid',
                'parent_class_name' => 'AdminLists'
            );
        }

        /*
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Customize the columns on the admin tabs');
        $this->description = $this->l('Customize the columns on the admin tabs');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall this module');

        $this->ps_versions_compliancy = array('min' => '1.6.0.0', 'max' => '8.0.99');

        if (version_compare(_PS_VERSION_, '1.7.7.0', '>=')) {
            $this->locale = Tools::getContextLocale($this->context);
        } elseif (version_compare(_PS_VERSION_, '1.7.6.0', '>=')) {
            $containerFinder = new PrestaShop\PrestaShop\Adapter\ContainerFinder($this->context);
            $container = $containerFinder->getContainer();
            if (null === $this->context->container) {
                $this->context->container = $container;
            }

            $localeRepository = $container->get(Tools::SERVICE_LOCALE_REPOSITORY);
            $this->locale = $localeRepository->getLocale(
                $this->context->language->getLocale()
            );
        }
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        if (!parent::install()) {
            return false;
        }

        include dirname(__FILE__) . '/sql/install.php';

        $tabs = array(
            'orders',
            'customer',
            'address',
            'customer_thread',
            'cart'
        );

        foreach ($tabs as $tab) {
            $this->insertTab($tab);
        }

        if (version_compare(_PS_VERSION_, '1.7.7.0', '>=')) {
            $this->registerHook('actionOrderGridQueryBuilderModifier');
            $this->registerHook('actionOrderGridDefinitionModifier');
            $this->registerHook('actionOrderGridDataModifier');

            $this->registerHook('actionAdminCartsListingFieldsModifier');

            $this->registerHook('actionAddressGridQueryBuilderModifier');
            $this->registerHook('actionAddressGridDefinitionModifier');
            $this->registerHook('actionAddressGridDataModifier');

            $this->registerHook('actionAdminCustomerThreadsListingFieldsModifier');
        } else {
            $this->registerHook('actionAdminOrdersListingFieldsModifier');
            $this->registerHook('actionAdminAddressesListingFieldsModifier');
            $this->registerHook('actionAdminCartsListingFieldsModifier');
            $this->registerHook('actionAdminCustomerThreadsListingFieldsModifier');
        }

        if (version_compare(_PS_VERSION_, '1.7.6.0', '<')) {
            $this->registerHook('actionAdminCustomersListingFieldsModifier');
        } else {
            $this->registerHook('actionCustomerGridQueryBuilderModifier');
            $this->registerHook('actionCustomerGridDefinitionModifier');
            $this->registerHook('actionCustomerGridDataModifier');
        }

        if (version_compare(_PS_VERSION_, '1.7.6.0', '>=')) {
            Tools::clearSf2Cache('prod');
            Tools::clearSf2Cache('dev');
        }

        return $this->addTab() &&
            $this->registerHook('displayBackOfficeHeader');
    }

    public function insertTab($tab)
    {
        $sql = 'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'extracolumns` (
           `tab`, `replace`) VALUES (\'' . pSQL($tab) . '\', \'0\')';

        if (Db::getInstance()->execute($sql) == false) {
            return false;
        }

        return true;
    }

    public function addTab()
    {
        if (Tab::getIdFromClassName('AdminLists') === false) {
            $tab = new Tab();
            $tab->class_name = 'AdminLists';
            $tab->icon = 'view_list';
            $tab->id_parent = (int)Tab::getIdFromClassName('IMPROVE');
            $tab->name[(int) Configuration::get('PS_LANG_DEFAULT')] = $this->l('Lists');
            $tab->add();
        }

        if (Tab::getIdFromClassName('AdminExtraColumns') === false) {
            $tab = new Tab();
            $tab->class_name = 'AdminExtraColumns';
            $tab->id_parent = (int)Tab::getIdFromClassName('AdminLists');
            $tab->module = $this->name;
            $tab->name[(int) Configuration::get('PS_LANG_DEFAULT')] = $this->l('Customize columns');
            $tab->add();
        }

        if (version_compare(_PS_VERSION_, '1.7.6.0', '>=') &&
            Tab::getIdFromClassName('AdminExtraColumnsGrid') === false) {
            $tab = new Tab();
            $tab->class_name = 'AdminExtraColumnsGrid';
            $tab->id_parent = (int)Tab::getIdFromClassName('AdminLists');
            $tab->module = $this->name;
            $tab->active = false;
            $tab->name[(int) Configuration::get('PS_LANG_DEFAULT')] = 'Extra columns grid';
            $tab->add();
        }

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall()) {
            return false;
        }

        include dirname(__FILE__) . '/sql/uninstall.php';

        return $this->removeTab();
    }

    private function removeTab()
    {
        $id_tabs = array(
            Tab::getIdFromClassName('AdminExtraColumns'),
            Tab::getIdFromClassName('AdminExtraColumnsGrid')
        );

        foreach ($id_tabs as $id_tab) {
            if ($id_tab) {
                $tab = new Tab($id_tab);
                if (Validate::isLoadedObject($tab)) {
                    $tab->delete();
                }
            }
        }

        return true;
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        $this->context->cookie->__unset('ec_list');
        if (Tools::getIsset('updateextracolumns')) {
            $link = new Link();
            $link = $link->getAdminLink('AdminExtraColumns');
            $this->context->cookie->__set('ec_list', (int)Tools::getValue('id_extracolumns'));
            Tools::redirectAdmin($link);
        }
        return $this->initList();
    }

    private function initList()
    {
        $this->fields_list = array(
            'id_extracolumns' => array(
                'title' => 'ID',
                'align' => 'text-center',
                'class' => 'fixed-width-xs',
                'filter_key' => 'id_extracolumns',
                'search' => false,
                'orderby' => false
            ),
            'tab' => array(
                'title' => $this->l('Tab'),
                'align' => 'text-center',
                'filter_key' => 'tab',
                'callback' => 'translateTabs',
                'callback_object' => $this,
                'search' => false,
                'orderby' => false
            ),
            'templates' => array(
                'title' => $this->l('Templates'),
                'align' => 'text-center',
                'filter_key' => 'b!name',
                'search' => false,
                'orderby' => false
            )
        );

        $list = Db::getInstance()->executeS(
            "SELECT `a`.*, GROUP_CONCAT(`b`.`name` SEPARATOR ', ') AS 'templates'
            FROM `" . _DB_PREFIX_ . $this->name . "` `a`
            LEFT JOIN `" . _DB_PREFIX_ . $this->name . "_profiles` `b`
                ON `a`.`id_extracolumns` = `b`.`id_extracolumns`
            GROUP BY `a`.`id_extracolumns`"
        );
        $helper = new HelperList();

        $helper->shopLinkType = '';

        // Actions to be displayed in the "Actions" column
        $helper->actions = array('edit');

        $helper->identifier = 'id_extracolumns';
        $helper->show_toolbar = true;
        $helper->title = $this->l('Choose the list you want to personalize');
        $helper->table = $this->name;

        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        return $helper->generateList($list, $this->fields_list);
    }

    public function translateTabs($value)
    {
        $value = Tools::ucfirst($value);
        // $this->l('Orders');
        // $this->l('Customer');
        // $this->l('Address');
        // $this->l('Cart');
        // $this->l('Customer_thread');
        return $this->l($value);
    }

    public function ajaxProcessActiveextracolumns()
    {
        $this->changeStatus('active');

        die(Tools::jsonEncode(array('success' => 1, 'message' => $this->l('Successful update.'))));
    }

    public function ajaxProcessReplaceextracolumns()
    {
        $this->changeStatus('replace');

        die(Tools::jsonEncode(array('success' => 1, 'message' => $this->l('Successful update.'))));
    }

    private function changeStatus($action)
    {
        $id = Tools::getValue('id_extracolumns');

        $value = (int)Db::getInstance()->getValue(
            "SELECT `" . $action . "`
            FROM `" . _DB_PREFIX_ . "extracolumns`
            WHERE `id_extracolumns` = '" . (int)$id . "'"
        );

        Db::getInstance()->execute(
            "UPDATE `" . _DB_PREFIX_ . "extracolumns`
            SET `" . pSQL($action) . "` = '" . !(int)$value . "'
            WHERE `id_extracolumns` = '" . (int)$id . "'"
        );
    }

    public function hookDisplayBackOfficeHeader()
    {
        if (!Module::isEnabled($this->name)) {
            return false;
        }

        $return = '';
        $form_name = '';
        $tab = '';
        $new_version = false;
        $controller = Tools::getValue('controller');

        switch ($controller) {
            case 'AdminOrders':
                $form_name = 'form-order';
                $tab = 'orders';
                break;
            case 'AdminCustomers':
                $form_name = 'form-customer';
                $tab = 'customer';
                break;
            case 'AdminAddresses':
                $form_name = 'form-address';
                $tab = 'address';
                break;
            case 'AdminCustomerThreads':
                $form_name = 'form-customer_thread';
                $tab = 'customer_thread';
                break;
            case 'AdminCarts':
                $form_name = 'form-cart';
                $tab = 'cart';
                break;

            default:
                if ($controller != 'AdminExtraColumns' && $controller != 'AdminExtraColumnsGrid') {
                    return false;
                }
                break;
        }

        if (!empty($tab)) {
            $extracolumns_list = new ExtraColumnsLists();
            if (!$extracolumns_list->isActive($tab)) {
                return false;
            }

            if (!$new_version && !empty($form_name)) {
                $profiles = array_merge(
                    array(
                        array(
                            'id_extracolumns_profiles' => 0,
                            'name' => $this->l('Default')
                        ),
                        array(
                            'id_extracolumns_profiles' => 1,
                            'name' => $this->l('Current config')
                        )
                    ),
                    ExtraColumnsProfile::getProfilesByTabName($tab)
                );
                $this->context->smarty->assign(array(
                    'form_name' => $form_name,
                    'profiles' => $profiles,
                    'ec_list' => $tab,
                    'ec_link' => $this->context->link,
                    'new_version' => $new_version
                ));
                $return .= $this->context->smarty->fetch(
                    $this->getLocalPath() . 'views/templates/admin/profiles.tpl'
                );
            }
        }

        return $return . '<script>
        var admin_extracolumns_ajax_url = ' . (string) json_encode(
            pSQL($this->context->link->getAdminLink('AdminExtraColumns'))
        ) . ';var current_id_tab = ' . (int) $this->context->controller->id . ';
        </script>';
    }

    public function hookActionAdminOrdersListingFieldsModifier($params)
    {
        $config = new ExtraColumnsConfiguration();
        $config = $config->getMinimalConfig('orders');

        if (!$config['active']) {
            return;
        }

        $extracolumns_lists = new ExtraColumnsLists();
        $extracolumns_lists->setUseParent(true)->setQuery('orders');

        if ($config['replace']) {
            $params['fields'] = array();
        } else {
            if (array_key_exists('reference', $params['fields'])) {
                $params['fields']['reference']['filter_key'] = "a!reference";
            }

            if (array_key_exists('total_paid_tax_incl', $params['fields'])) {
                $params['fields']['total_paid_tax_incl']['filter_key'] = "a!total_paid_tax_incl";
            }

            if (array_key_exists('payment', $params['fields'])) {
                $params['fields']['payment']['filter_key'] = "a!payment";
            }

//            if (array_key_exists('new', $params['fields'])) {
//                $params['fields']['new']['search'] = false;
//            }
        }

        if (array_key_exists('select', $params)) {
            $params['select'] .= ', ' . implode(', ', $extracolumns_lists->getSelect());
        } else {
            $params['select'] = implode(', ', $extracolumns_lists->getSelect());
        }

        if (array_key_exists('join', $params)) {
            $params['join'] .= ' ' . implode(' ', $extracolumns_lists->getJoin());
        } else {
            $params['join'] = ' ' . implode(' ', $extracolumns_lists->getJoin());
        }


        $params['group_by'] = 'GROUP BY `a`.`id_order`';

        $fields = $extracolumns_lists->getFieldsBefore177('orders');

        if (!$config['replace'] && $config['start'] == 'id') {
            $fields_tmp = $params['fields'];
            $params['fields'] = array_slice($fields_tmp, 0, 1, true) +
            $fields +
            array_slice($fields_tmp, 1, count($fields_tmp)-1, true);
        } else {
            $params['fields'] += $fields;
        }

//        foreach ($fields as $key => $field) {
//            $params['fields'][$key] = $field;
//        }

        if (array_key_exists('id_pdf', $params['fields'])) {
            unset($params['fields']['id_pdf']);
        }

        $params['fields']['id_pdf'] =
            array(
                'title' => 'PDF',
                'callback' => 'printPDFIcons',
                'remove_onclick' => '1',
                'class' => 'fixed-width-xs',
                'align' => 'text-center'
            );
    }

    public function hookActionAdminCustomersListingFieldsModifier($params)
    {
        $config = new ExtraColumnsConfiguration();
        $config = $config->getMinimalConfig('customer');

        if (!$config['active']) {
            return;
        }

        $extracolumns_lists = new ExtraColumnsLists();
        $extracolumns_lists->setUseParent(true)->setQuery('customer');

        if ($config['replace']) {
            $params['fields'] = array();
        } else {
            if (array_key_exists('id_customer', $params['fields'])) {
                $params['fields']['id_customer']['filter_key'] = 'a!id_customer';
            }
            if (array_key_exists('newsletter', $params['fields'])) {
                $params['fields']['newsletter']['filter_key'] = 'a!newsletter';
            }
            if (array_key_exists('optin', $params['fields'])) {
                $params['fields']['optin']['filter_key'] = 'a!optin';
            }
            if (array_key_exists('date_add', $params['fields'])) {
                $params['fields']['date_add']['filter_key'] = 'a!date_add';
            }
            if (array_key_exists('firstname', $params['fields'])) {
                $params['fields']['firstname']['filter_key'] = 'a!firstname';
            }
            if (array_key_exists('lastname', $params['fields'])) {
                $params['fields']['lastname']['filter_key'] = 'a!lastname';
            }
            if (array_key_exists('email', $params['fields'])) {
                $params['fields']['email']['filter_key'] = 'a!email';
            }
            if (array_key_exists('company', $params['fields'])) {
                $params['fields']['company']['filter_key'] = 'a!company';
            }
        }

        $get_select = $extracolumns_lists->getSelect();
        if (!empty($get_select) && array_key_exists('select', $params)) {
            $params['select'] .= ', ' . implode(', ', $get_select);
        } elseif (!empty($get_select)) {
            $params['select'] = implode(', ', $get_select);
        }

        if (array_key_exists('join', $params)) {
            $params['join'] .= ' ' . implode(' ', $extracolumns_lists->getJoin());
        } else {
            $params['join'] = ' ' . implode(' ', $extracolumns_lists->getJoin());
        }

        $params['group_by'] = 'GROUP BY `a`.`id_customer`';

        $fields = $extracolumns_lists->getFieldsBefore177('customer');

        if (!$config['replace'] && $config['start'] == 'id') {
            $fields_tmp = $params['fields'];
            $params['fields'] = array_slice($fields_tmp, 0, 1, true) +
                $fields +
                array_slice($fields_tmp, 1, count($fields_tmp)-1, true);
        } else {
            $params['fields'] += $fields;
        }

//        foreach ($fields as $key => $field) {
//            $params['fields'][$key] = $field;
//        }
    }

    public function hookActionAdminAddressesListingFieldsModifier($params)
    {
        $config = new ExtraColumnsConfiguration();
        $config = $config->getMinimalConfig('address');

        if (!$config['active']) {
            return;
        }

        $extracolumns_lists = new ExtraColumnsLists();
        $extracolumns_lists->setUseParent(true)->setQuery('address');

        if ($config['replace']) {
            $params['fields'] = array();
        }

        if (array_key_exists('select', $params)) {
            $params['select'] .= ', ' . implode(', ', $extracolumns_lists->getSelect());
        } else {
            $params['select'] = implode(', ', $extracolumns_lists->getSelect());
        }

        if (array_key_exists('join', $params)) {
            $params['join'] .= ' ' . implode(' ', $extracolumns_lists->getJoin());
        } else {
            $params['join'] = ' ' . implode(' ', $extracolumns_lists->getJoin());
        }


        $params['group_by'] = 'GROUP BY `a`.`id_address`';

        $fields = $extracolumns_lists->getFieldsBefore177('address');

        if (!$config['replace'] && $config['start'] == 'id') {
            $fields_tmp = $params['fields'];
            $params['fields'] = array_slice($fields_tmp, 0, 1, true) +
                $fields +
                array_slice($fields_tmp, 1, count($fields_tmp)-1, true);
        } else {
            $params['fields'] += $fields;
        }

//        foreach ($fields as $key => $field) {
//            $params['fields'][$key] = $field;
//        }
    }

    public function hookActionAdminCartsListingFieldsModifier($params)
    {
        $config = new ExtraColumnsConfiguration();
        $config = $config->getMinimalConfig('cart');

        if (!$config['active']) {
            return;
        }

        $extracolumns_lists = new ExtraColumnsLists();
        $extracolumns_lists->setUseParent(true)->setQuery('cart');

        if ($config['replace']) {
            $params['fields'] = array();
        }

        if (array_key_exists('select', $params)) {
            $params['select'] .= ', ' . implode(', ', $extracolumns_lists->getSelect());
        } else {
            $params['select'] = implode(', ', $extracolumns_lists->getSelect());
        }

        if (array_key_exists('join', $params)) {
            $params['join'] .= ' ' . implode(' ', $extracolumns_lists->getJoin());
        } else {
            $params['join'] = ' ' . implode(' ', $extracolumns_lists->getJoin());
        }


        $params['group_by'] = 'GROUP BY `a`.`id_cart`';

        $fields = $extracolumns_lists->getFieldsBefore177('cart');

        if (!$config['replace'] && $config['start'] == 'id') {
            $fields_tmp = $params['fields'];
            $params['fields'] = array_slice($fields_tmp, 0, 1, true) +
                $fields +
                array_slice($fields_tmp, 1, count($fields_tmp)-1, true);
        } else {
            $params['fields'] += $fields;
        }

//        foreach ($fields as $key => $field) {
//            $params['fields'][$key] = $field;
//        }
    }

    public function hookActionAdminCustomerThreadsListingFieldsModifier($params)
    {
        $config = new ExtraColumnsConfiguration();
        $config = $config->getMinimalConfig('customer_thread');

        if (!$config['active']) {
            return;
        }

        $extracolumns_lists = new ExtraColumnsLists();
        $extracolumns_lists->setUseParent(true)->setQuery('customer_thread');

        if ($config['replace']) {
            $params['fields'] = array();
        }

        if (array_key_exists('select', $params)) {
            $params['select'] .= ', ' . implode(', ', $extracolumns_lists->getSelect());
        } else {
            $params['select'] = implode(', ', $extracolumns_lists->getSelect());
        }

        if (array_key_exists('join', $params)) {
            $params['join'] .= ' ' . implode(' ', $extracolumns_lists->getJoin());
        } else {
            $params['join'] = ' ' . implode(' ', $extracolumns_lists->getJoin());
        }


        $params['group_by'] = 'GROUP BY `a`.`id_customer_thread`';

        $fields = $extracolumns_lists->getFieldsBefore177('customer_thread');

        if (!$config['replace'] && $config['start'] == 'id') {
            $fields_tmp = $params['fields'];
            $params['fields'] = array_slice($fields_tmp, 0, 1, true) +
                $fields +
                array_slice($fields_tmp, 1, count($fields_tmp)-1, true);
        } else {
            $params['fields'] += $fields;
        }

//        foreach ($fields as $key => $field) {
//            $params['fields'][$key] = $field;
//        }
    }

    /**
     * @param mixed[] $params
     */
    public function hookActionOrderGridQueryBuilderModifier($params)
    {
        $extracolumns_lists = new ExtraColumnsLists();

        if (!$extracolumns_lists->isActive('orders')) {
            return;
        }

        $extracolumns_lists->setUseParent(false)->setQuery('orders');

        $params['search_query_builder']->addSelect($extracolumns_lists->getSelect());

        $countQueryBuilder = clone $params['count_query_builder'];
        $countQueryBuilder->addSelect($extracolumns_lists->getSelect());
        $countQueryBuilder->addSelect("`" . $extracolumns_lists->aliases['orders'] . "`.`id_order`");

        foreach ($extracolumns_lists->getJoin() as $join) {
            $callback = $join['type'] . "Join";
            $params['search_query_builder']->$callback(
                $join['fromAlias'],
                _DB_PREFIX_ . $join['join'],
                $join['alias'],
                $join['condition']
            );

            $countQueryBuilder->$callback(
                $join['fromAlias'],
                _DB_PREFIX_ . $join['join'],
                $join['alias'],
                $join['condition']
            );
        }

        $extracolumns_lists->setHaving(
            'orders',
            $params['search_criteria'],
            $params['search_query_builder']
        );

        $extracolumns_lists->setHaving(
            'orders',
            $params['search_criteria'],
            $countQueryBuilder
        );

        $params['search_query_builder']->addGroupBy($extracolumns_lists->aliases['orders'] . '.id_order');
        $countQueryBuilder->addGroupBy($extracolumns_lists->aliases['orders'] . '.id_order');

        $params['count_query_builder']->resetQueryParts(array(
            'from',
            'join',
            'where'
        ));

        $params['count_query_builder']->from("(" . $countQueryBuilder . ")", $extracolumns_lists->aliases['orders']);
    }

    /**
     * @param \PrestaShop\PrestaShop\Core\Grid\Definition\GridDefinition[] $params
     */
    public function hookActionOrderGridDefinitionModifier($params)
    {
        $extracolumns_lists = new ExtraColumnsLists();

        if (!$extracolumns_lists->isActive('orders')) {
            return;
        }

        $extracolumns_lists->addExportGridAction($params['definition']);

        $extracolumns_lists->getFields('orders', $this, $params['definition']);
    }

    /**
     * @param PrestaShop\PrestaShop\Core\Grid\Data\GridData[] $params
     */
    public function hookActionOrderGridDataModifier($params)
    {
        $extracolumns_lists = new ExtraColumnsLists();

        if (!$extracolumns_lists->isActive('orders')) {
            return;
        }

        $columns = $extracolumns_lists->getColumns('orders');

        $records = $params['data']->getRecords()->all();

        if (!empty($columns)) {
            foreach ($records as &$record) {
                foreach ($columns as $column) {
                    $key = $extracolumns_lists->getAttributeAs($column['table'], $column['attribute']);

                    if (!array_key_exists($key, $record)) {
                        continue;
                    }

                    if ($key == 'order_detailproduct_weight' || $key === "extracolumnssum_discount") {
                        $record[$key] = $extracolumns_lists->printSum($record[$key], 2);
                    } elseif ($key == 'order_detailproduct_quantity') {
                        $record[$key] = $extracolumns_lists->printSum($record[$key], 0);
                    } elseif ($key == 'imageid_image') {
                        $record['ec_products_infos'] = $extracolumns_lists->getProductsImageInfos($record[$key], $record);
                    }

                    if ($column['type'] == 'price') {
                        $record[$key] = $extracolumns_lists->modifyToPrice(
                            $record[$key],
                            $record,
                            $this->locale
                        );
                    } elseif ($column['type'] == 'percent') {
                        $record[$key] =
                            $extracolumns_lists->{'modifyTo' . Tools::ucfirst($column['type'])}(
                                $record[$key],
                                $record
                            );
                    }
                }
            }
        }

        $params['data'] = new PrestaShop\PrestaShop\Core\Grid\Data\GridData(
            new PrestaShop\PrestaShop\Core\Grid\Record\RecordCollection($records),
            $params['data']->getRecordsTotal(),
            $params['data']->getQuery()
        );
    }

    /**
     * @param mixed[] $params
     */
    public function hookActionCustomerGridQueryBuilderModifier($params)
    {
        if (array_key_exists('route', $params) && $params['route'] == "admin_customers_view") {
            return;
        }
        $extracolumns_lists = new ExtraColumnsLists();

        if (!$extracolumns_lists->isActive('customer')) {
            return;
        }

        $extracolumns_lists->setUseParent(false)->setQuery('customer');

        $params['search_query_builder']->addSelect($extracolumns_lists->getSelect());

        $countQueryBuilder = clone $params['count_query_builder'];
        $countQueryBuilder->addSelect($extracolumns_lists->getSelect());
        $countQueryBuilder->addSelect("`" . $extracolumns_lists->aliases['customer'] . "`.`id_customer`");

        foreach ($extracolumns_lists->getJoin() as $join) {
            $callback = $join['type'] . "Join";
            $params['search_query_builder']->$callback(
                $join['fromAlias'],
                _DB_PREFIX_ . $join['join'],
                $join['alias'],
                $join['condition']
            );

            $countQueryBuilder->$callback(
                $join['fromAlias'],
                _DB_PREFIX_ . $join['join'],
                $join['alias'],
                $join['condition']
            );
        }

        $extracolumns_lists->setHaving(
            'customer',
            $params['search_criteria'],
            $params['search_query_builder']
        );

        $extracolumns_lists->setHaving(
            'customer',
            $params['search_criteria'],
            $countQueryBuilder
        );

        $params['search_query_builder']->addGroupBy($extracolumns_lists->aliases['customer'] . '.id_customer');
        $countQueryBuilder->addGroupBy($extracolumns_lists->aliases['customer'] . '.id_customer');

        $params['count_query_builder']->resetQueryParts(array(
            'from',
            'join',
            'where'
        ));

        $params['count_query_builder']->from("(" . $countQueryBuilder . ")", $extracolumns_lists->aliases['customer']);
    }

    /**
     * @param \PrestaShop\PrestaShop\Core\Grid\Definition\GridDefinition[] $params
     */
    public function hookActionCustomerGridDefinitionModifier($params)
    {
        $extracolumns_lists = new ExtraColumnsLists();

        if (!$extracolumns_lists->isActive('customer')) {
            return;
        }

        $extracolumns_lists->addExportGridAction($params['definition']);

        $extracolumns_lists->getFields('customer', $this, $params['definition']);
    }

    /**
     * @param PrestaShop\PrestaShop\Core\Grid\Data\GridData[] $params
     */
    public function hookActionCustomerGridDataModifier($params)
    {
        $extracolumns_lists = new ExtraColumnsLists();

        if (!$extracolumns_lists->isActive('customer')) {
            return;
        }

        $columns = $extracolumns_lists->getColumns('customer', true);

        $records = $params['data']->getRecords()->all();

        if (!empty($columns)) {
            foreach ($records as &$record) {
                foreach ($columns as $column) {
                    $key = $column['table'] . $column['attribute'];

                    if (!array_key_exists($key, $record)) {
                        continue;
                    }

                    if ($column['type'] == 'price') {
                        $record[$key] = $extracolumns_lists->modifyToPrice(
                            $record[$key],
                            $record,
                            $this->locale
                        );
                    } else {
                        $record[$key] =
                            $extracolumns_lists->{'modifyTo' . Tools::ucfirst($column['type'])}(
                                $record[$key],
                                $record
                            );
                    }
                }
            }
        }

        $params['data'] = new PrestaShop\PrestaShop\Core\Grid\Data\GridData(
            new PrestaShop\PrestaShop\Core\Grid\Record\RecordCollection($records),
            $params['data']->getRecordsTotal(),
            $params['data']->getQuery()
        );
    }

    /**
     * @param mixed[] $params
     */
    public function hookActionAddressGridQueryBuilderModifier($params)
    {
        $extracolumns_lists = new ExtraColumnsLists();

        if (!$extracolumns_lists->isActive('address')) {
            return;
        }

        $extracolumns_lists->setUseParent(false)->setQuery('address');

        $params['search_query_builder']->addSelect($extracolumns_lists->getSelect());

        $countQueryBuilder = clone $params['count_query_builder'];
        $countQueryBuilder->addSelect($extracolumns_lists->getSelect());
        $countQueryBuilder->addSelect("`" . $extracolumns_lists->aliases['address'] . "`.`id_address`");

        foreach ($extracolumns_lists->getJoin() as $join) {
            $callback = $join['type'] . "Join";
            $params['search_query_builder']->$callback(
                $join['fromAlias'],
                _DB_PREFIX_ . $join['join'],
                $join['alias'],
                $join['condition']
            );

            $countQueryBuilder->$callback(
                $join['fromAlias'],
                _DB_PREFIX_ . $join['join'],
                $join['alias'],
                $join['condition']
            );
        }

        $extracolumns_lists->setHaving(
            'address',
            $params['search_criteria'],
            $params['search_query_builder']
        );

        $extracolumns_lists->setHaving(
            'address',
            $params['search_criteria'],
            $countQueryBuilder
        );

        $params['search_query_builder']->addGroupBy($extracolumns_lists->aliases['address'] . '.id_address');
        $countQueryBuilder->addGroupBy($extracolumns_lists->aliases['address'] . '.id_address');

        $params['count_query_builder']->resetQueryParts(array(
            'from',
            'join',
            'where'
        ));

        $params['count_query_builder']->from("(" . $countQueryBuilder . ")", $extracolumns_lists->aliases['address']);
    }

    /**
     * @param \PrestaShop\PrestaShop\Core\Grid\Definition\GridDefinition[] $params
     */
    public function hookActionAddressGridDefinitionModifier($params)
    {
        $extracolumns_lists = new ExtraColumnsLists();

        if (!$extracolumns_lists->isActive('address')) {
            return;
        }

        $extracolumns_lists->getFields('address', $this, $params['definition']);
    }

    /**
     * @param PrestaShop\PrestaShop\Core\Grid\Data\GridData[] $params
     */
    public function hookActionAddressGridDataModifier($params)
    {
        $extracolumns_lists = new ExtraColumnsLists();

        if (!$extracolumns_lists->isActive('address')) {
            return;
        }

        $columns = $extracolumns_lists->getColumns('address', true);

        $records = $params['data']->getRecords()->all();

        if (!empty($columns)) {
            foreach ($records as &$record) {
                foreach ($columns as $column) {
                    $key = $column['table'] . $column['attribute'];

                    if (!array_key_exists($key, $record)) {
                        continue;
                    }

                    if ($column['type'] == 'price') {
                        $record[$key] = $extracolumns_lists->modifyToPrice(
                            $record[$key],
                            $record,
                            $this->locale
                        );
                    } else {
                        $record[$key] =
                            $extracolumns_lists->{'modifyTo' . Tools::ucfirst($column['type'])}(
                                $record[$key],
                                $record
                            );
                    }
                }
            }
        }

        $params['data'] = new PrestaShop\PrestaShop\Core\Grid\Data\GridData(
            new PrestaShop\PrestaShop\Core\Grid\Record\RecordCollection($records),
            $params['data']->getRecordsTotal(),
            $params['data']->getQuery()
        );
    }
}
