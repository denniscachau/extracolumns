<?php
/**
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class ExtraColumnsConfiguration extends ObjectModel
{
    /** @var string Name */
    public $id_extracolumns;
    public $position;
    public $table;
    public $attribute;
    public $title;
    public $type;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'extracolumns_columns',
        'primary' => 'id_extracolumns_columns',
        'multilang' => false,
        'fields' => array(
          'id_extracolumns' => array(
            'type' => self::TYPE_STRING, 'validate' => 'isInt', 'required' => false
          ),
          'position' => array(
            'type' => self::TYPE_STRING, 'required' => false
          ),
          'table' => array(
            'type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => false
          ),
          'attribute' => array(
            'type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => false
          ),
          'title' => array(
            'type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => false
          ),
          'type' => array(
            'type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => false
          ),
        ),
    );

    public function updatePosition($way, $position)
    {
        $id_extracolumns = (int)Context::getContext()->cookie->ec_list;
        $id_extracolumns_columns = Configuration::get('id_extracolumns_columns');
        // TODO use the id_extracolumns from the following query
        if (!$res = Db::getInstance()->executeS(
            "SELECT `id_extracolumns_columns`, `position`
            FROM `" . _DB_PREFIX_ . "extracolumns_columns`
            WHERE `id_extracolumns` = '" . (int)$id_extracolumns . "' AND `id_extracolumns_profiles` = 1
            ORDER BY `position` ASC"
        )) {
            return false;
        }

        if (version_compare(_PS_VERSION_, '1.6.1.0', '<')) {
            $buffer_position = 1;
        } else {
            $buffer_position = 0;
        }

        foreach ($res as $columns) {
            if ((int)$columns['position'] != $buffer_position) {
                Db::getInstance()->execute(
                    "UPDATE `" . _DB_PREFIX_ . "extracolumns_columns`
                    SET `position` = '" . (int)$buffer_position . "'
                    WHERE `id_extracolumns_columns` = '" . (int)$columns['id_extracolumns_columns'] . "'"
                );
            }
            $buffer_position++;
            if ((int)$columns['id_extracolumns_columns'] == (int)$id_extracolumns_columns) {
                $moved_columns = Db::getInstance()->getRow(
                    "SELECT `id_extracolumns_columns`, `position`
                    FROM `" . _DB_PREFIX_ . "extracolumns_columns`
                    WHERE `id_extracolumns_columns` = '" . (int)$id_extracolumns_columns . "'"
                );
            }
        }

        if (!isset($moved_columns) || !isset($position)) {
            return false;
        }
        // < and > statements rather than BETWEEN operator
        // since BETWEEN is treated differently according to databases
        return (Db::getInstance()->execute("
            UPDATE `" . _DB_PREFIX_ . "extracolumns_columns`
            SET `position`= `position` " . ($way ? "- 1" : "+ 1") . "
            WHERE `id_extracolumns` = '" . (int)$id_extracolumns . "' AND `id_extracolumns_profiles` = 1 AND `position`
            " . ($way
                ? "> " . (int)$moved_columns['position'] . " AND `position` <= " . (int)$position
                : "< " . (int)$moved_columns['position']." AND `position` >= " . (int)$position . "
            "))
        && Db::getInstance()->execute('
            UPDATE `'._DB_PREFIX_.'extracolumns_columns`
            SET `position` = '.(int)$position.'
            WHERE `id_extracolumns_columns` = '.(int)$moved_columns['id_extracolumns_columns']));
    }

    public function populateExtracolumns($id, $replace, $start = 'id')
    {
        return Db::getInstance()->execute(
            "UPDATE `" . _DB_PREFIX_ . "extracolumns`
            SET `replace` = '" . (int)(bool)$replace . "', `start` = '" . pSQL($start) . "'
            WHERE `id_extracolumns` = '" . (int)$id . "'"
        );
    }

    public function changePositions()
    {
        $id_extracolumns = (int)Context::getContext()->cookie->ec_list;
        if (!$res = Db::getInstance()->executeS(
            "SELECT `id_extracolumns_columns`, `position`
            FROM `" . _DB_PREFIX_ . "extracolumns_columns`
            WHERE `id_extracolumns` = '" . (int)$id_extracolumns . "' AND `id_extracolumns_profiles` = 1
            ORDER BY `position` ASC"
        )) {
            return false;
        }

        if (version_compare(_PS_VERSION_, '1.6.1.0', '<')) {
            $buffer_position = 1;
        } else {
            $buffer_position = 0;
        }

        foreach ($res as $columns) {
            if ((int)$columns['position'] != $buffer_position) {
                if (!Db::getInstance()->execute(
                    "UPDATE `" . _DB_PREFIX_ . "extracolumns_columns`
                    SET `position` = '" . (int)$buffer_position . "'
                    WHERE `id_extracolumns_columns` = '" . (int)$columns['id_extracolumns_columns'] . "'"
                )) {
                    return false;
                }
            }
            $buffer_position++;
        }
        return true;
    }

    public static function getLastPosition($id)
    {
        return Db::getInstance()->getValue(
            "SELECT IF(MAX(`position`) IS NULL, 0, MAX(`position`)+1)
            FROM `" . _DB_PREFIX_ . "extracolumns_columns`
            WHERE `id_extracolumns` = '" . (int)$id . "' AND `id_extracolumns_profiles` = 1"
        );
    }

    public function add($auto_date = true, $null_values = false)
    {
        if (isset($this->id) && !$this->force_id) {
            unset($this->id);
        }

        // Automatically fill dates
        if ($auto_date && property_exists($this, 'date_add')) {
            $this->date_add = date('Y-m-d H:i:s');
        }
        if ($auto_date && property_exists($this, 'date_upd')) {
            $this->date_upd = date('Y-m-d H:i:s');
        }

        if (empty(Tools::getValue('attributes'))) {
            $this->id = 1;
            return true;
        }

        $positions = explode(';', Tools::getValue('positions'));
        $attributes = explode(';', Tools::getValue('attributes'));
        $tables = explode(';', Tools::getValue('tables'));
        $titles = explode(';', Tools::getValue('titles'));
        $types = explode(';', Tools::getValue('types'));

        foreach ($attributes as $key => $attribute) {
            // $_POST['position'] = $positions[$key];
            // $_POST['attribute'] = $attribute;
            // $_POST['table'] = $tables[$key];
            // $_POST['type'] = $types[$key];
            // $_POST['title'] = $titles[$key];

            if (version_compare(_PS_VERSION_, '1.6.1.0', '<')) {
                $positions[$key] -= 1;
            }

            $fields_to_insert = array(
                'id_extracolumns' => (int)Context::getContext()->cookie->ec_list,
                'id_extracolumns_profiles' => 1,
                'position' => (int)$positions[$key],
                'attribute' => pSQL($attribute),
                'table' => pSQL($tables[$key]),
                'type' => pSQL($types[$key]),
                'title' => pSQL($titles[$key])
            );

            if (!$result = Db::getInstance()->insert(
                $this->def['table'],
                $fields_to_insert,
                $null_values,
                true,
                Db::INSERT_IGNORE
            )) {
                return false;
            }
        }
        $this->id = Db::getInstance()->Insert_ID();

        if (!$result) {
            return false;
        }

        return $result;
    }

    public function delete()
    {
        $return = parent::delete();
        $this->changePositions();
        return $return;
    }

    public function getMinimalConfig($tab)
    {
        $config = Db::getInstance()->getRow(
            "SELECT IF(
                `ep`.`id_extracolumns_profiles` <> 0 OR `ep`.`id_extracolumns_profiles` IS NULL,
                IF(
                    `ep`.`id_extracolumns_profiles` = 1 OR `ep`.`id_extracolumns_profiles` IS NULL,
                    `a`.`replace`,
                    `p`.`replace`
                ),
                0
            ) AS 'replace', IF(
                `ep`.`id_extracolumns_profiles` <> 0 OR `ep`.`id_extracolumns_profiles` IS NULL,
                IF(
                    `ep`.`id_extracolumns_profiles` = 1 OR `ep`.`id_extracolumns_profiles` IS NULL,
                    `a`.`start`,
                    `p`.`start`
                ),
                0
            ) AS 'start'
            FROM `" . _DB_PREFIX_ . "extracolumns` `a`
            LEFT JOIN `" . _DB_PREFIX_ . "extracolumns_employee_profile` `ep`
                ON `a`.`id_extracolumns` = `ep`.`id_extracolumns`
                    AND `ep`.`id_employee` = " . (int)Context::getContext()->employee->id . "
            LEFT JOIN `" . _DB_PREFIX_ . "extracolumns_profiles` `p`
                ON `ep`.`id_extracolumns_profiles` = `p`.`id_extracolumns_profiles`
            WHERE `a`.`tab` = '" . pSQL($tab) . "'"
        );

        return array(
            'active' => true,
            'replace' => $config['replace'],
            'start' => $config['start']
        );
    }

    public static function listExists($id_list) {
        $table = "extracolumns";

        $id = Db::getInstance()->getValue(
            'SELECT `id_' . bqSQL($table) . '` as id
			FROM `' . _DB_PREFIX_ . bqSQL($table) . '` e
			WHERE e.`id_' . bqSQL($table) . '` = ' . (int) $id_list,
            false
        );

        return isset($id);
    }
}
