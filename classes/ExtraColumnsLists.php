<?php
/**
 * 2007-2023 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2023 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

class ExtraColumnsLists extends DbQueryCore
{
    private $configuration_table = "extracolumns";

    public $select_fields = array();

    private $tables_default = array();

    private $fields = array();

    public $aliases = array();

    private $context;

    private $attributes_as_list = array();

    private $already_join = array();

    private $use_parent = false;

    private $replace = null;

    public function __construct()
    {
        $this->context = Context::getContext();

        if (version_compare(_PS_VERSION_, '1.7.6.0', '<')) {
            $this->attributes_as_list['extracolumnslast_visit'] = 'connect';
            $this->attributes_as_list['extracolumnstotal_spent'] = 'total_spent';
        }

        if (version_compare(_PS_VERSION_, '1.7.7.0', '<')) {
            $this->attributes_as_list['extracolumnsnew_client'] = 'new';
            $this->attributes_as_list['extracolumnscustomer'] = 'customer';
        }
    }

    public function setUseParent($use_parent)
    {
        $this->use_parent = (bool)$use_parent;
        return $this;
    }

    public function setQuery($tab)
    {
        $get_configs = $this->getConfigurations($tab);

        if (empty($get_configs)) {
            return 0;
        }

        $this->getParameters($tab);
        $array_attributes = $get_configs['array_attributes'];
        $config_tables = $get_configs['config_tables'];
        $array_tables = $get_configs['array_tables'];

        $sum_attributes = array();

//        $this->already_join = $this->tables_default;
        $this->already_join[] = "extracolumns";
        $this->setDefaultJoin($tab);

        switch ($tab) {
            case 'orders':
                $sum_attributes = array(
                    'order_detailproduct_weight',
                    'order_detailproduct_quantity'
                );
                break;
        }

        $tables = array_unique($config_tables);
        $ndefault_tables = array();

        foreach ($tables as $table) {
            foreach ($this->tables_default as $table_default) {
                switch ($table) {
                    case $table_default:
                        $this->addDefaultJoin($tab, $table_default);
                        continue 3;
                    case 'billing_address':
                        $this->joinQuery(
                            'left',
                            $this->getAlias('orders'),
                            'address',
                            'billing_address',
                            '`' . $this->getAlias('orders') . '`.`id_address_invoice` = `billing_address`.`id_address`'
                        );
                        $this->already_join[] = $table;
                        continue 3;
                }
            }
            $ndefault_tables[] = $table;
        }

        foreach ($ndefault_tables as $value) {
            if (empty($value) || in_array($value, $this->already_join)) {
                continue;
            }
            $columns = Db::getInstance()->executeS(
                "SHOW COLUMNS FROM `" . _DB_PREFIX_ . pSQL($value) . "`"
            );
            $base_on = '';
            $join_on = '';
            $join_table = '';
            $lang = false;

            foreach ($this->fields as $join_table_temp => $field) {
                if (is_array($field)) {
                    $join_table_temp = $field['table'];
                } else {
                    $field = array(
                        'base_on' => $field,
                        'join_on' => $field,
                    );
                }

                foreach ($columns as $column) {
                    if ($column['Field'] == $field['join_on'] && $join_on == '') {
                        $base_on = $field['base_on'];
                        $join_on = $field['join_on'];
                        $join_table = $join_table_temp;
                        $this->addDefaultJoin($tab, $join_table);
                    } elseif ($column['Field'] == 'id_lang') {
                        $lang = true;
                    }

                    if ($lang && $join_on != '') {
                        break 2;
                    }
                }
            }
            $this->already_join[] = $value;
            $on = '`' . pSQL($value) . '`.`' . pSQL($join_on) . '` =
                `' . pSQL($join_table) . '`.`' . pSQL($base_on) . '`';
            if ($lang && preg_match('/.*_lang$/', $value)) {
                $on .= " AND `" . pSQL($value) . "`.`id_lang` = '" . (int)$this->context->language->id . "'";
            }

            $this->joinQuery(
                'left',
                pSQL($join_table),
                pSQL($value),
                pSQL($value),
                $on
            );
        }

        foreach ($array_attributes as $key => $attribute) {
            if (!array_key_exists($key, $config_tables) || empty($config_tables[$key]) || empty($attribute)) {
                continue;
            }

            $attribute_as = $this->getAttributeAs($config_tables[$key], $attribute);

            $default_select = false;
            if ($array_tables[$key] == "product" || $array_tables[$key] == "product_lang") {
                if ($tab == 'cart') {
                    $product_quantity = "`cart_product`.`quantity`";
                } else {
                    $product_quantity = "`order_detail`.`product_quantity`";
                }
                $this->select("GROUP_CONCAT(DISTINCT IF(" . $product_quantity . " > 1,
                    CONCAT(" . $product_quantity . ", 'x ',
                    `" . pSQL($array_tables[$key]) . "`.`" . pSQL($attribute) . "`),
                    `" . pSQL($array_tables[$key]) . "`.`" . pSQL($attribute) . "`)
                    SEPARATOR '; ') AS '" . $attribute_as . "'");
            } elseif ($array_tables[$key] != 'extracolumns') {
                switch ($tab) {
                    case 'orders':
                        switch ($attribute_as) {
                            case 'customized_data-value':
                                $this->select("GROUP_CONCAT(DISTINCT
                                    CONCAT(`product`.`reference`,
                                    ': ',`" . pSQL($array_tables[$key]) . "`.`" . pSQL($attribute) ."`) SEPARATOR '; ')
                                    AS '" . $attribute_as . "'");
                                break;

                            case 'order_state_langname':
                                $this->select("`os`.`color`");
                                $default_select = true;
                                break;


                            case 'order_carriertracking_number':
                                $this->addDefaultJoin($tab, 'carrier');
                                $this->select('`carrier`.`url` AS "carrier_url"');
                                $default_select = true;
                                break;

                            case (preg_match('/cart_rule\w+/', $attribute_as) ? $attribute_as : false):
                                if (!in_array('order_cart_rule', $this->already_join)) {
                                    $on = "`" . $this->getAlias('orders') . "`.`id_order` =
                                        `order_cart_rule`.`id_order`";
                                    $this->joinQuery(
                                        'left',
                                        $this->getAlias('orders'),
                                        'order_cart_rule',
                                        'order_cart_rule',
                                        $on
                                    );
                                    $this->already_join[] = 'order_cart_rule';
                                }
                                if (!in_array('cart_rule2', $this->already_join)) {
                                    $on = "`order_cart_rule`.`id_cart_rule` =
                                        `cart_rule`.`id_cart_rule`";
                                    $this->joinQuery(
                                        'left',
                                        'order_cart_rule',
                                        'cart_rule',
                                        'cart_rule',
                                        $on
                                    );
                                    $this->already_join[] = 'cart_rule2';
                                }
                                $default_select = true;
                                break;
                            case 'imageid_image':
                                $this->select("GROUP_CONCAT(DISTINCT 
                                CONCAT(`order_detail`.`product_quantity`, ':', IF(
                                    `product_attribute_image`.`id_product_attribute` IS NULL 
                                        OR `product_attribute_image`.`id_image` = 0,
                                    `" . $this->getAlias('image') . "`.`id_image`,
                                    `product_attribute_image`.`id_image`
                                )) ORDER BY `order_detail`.`id_order_detail` SEPARATOR ';') AS '" . $attribute_as . "'");

//                                $this->select("GROUP_CONCAT(DISTINCT
//                                    CONCAT(`order_detail`.`product_quantity`,
//                                    ':',
//                                    `" . pSQL($array_tables[$key]) . "`.`" . pSQL($attribute) ."`
//                                    ) ORDER BY `order_detail`.`id_order_detail` SEPARATOR ';') AS '" . $attribute_as . "'");
                                $this->select("GROUP_CONCAT(DISTINCT CONCAT(`order_detail`.`id_order_detail`, ':', `order_detail`.`product_id`) ORDER BY `order_detail`.`id_order_detail` SEPARATOR ';') AS 'ec_image_id_product'");
                                $this->select("GROUP_CONCAT(DISTINCT CONCAT(`order_detail`.`id_order_detail`, ':', `order_detail`.`product_attribute_id`) ORDER BY `order_detail`.`id_order_detail` SEPARATOR ';') AS 'ec_image_id_product_attribute'");
                                break;
                            default:
                                if (in_array($attribute_as, $sum_attributes)) {
                                    $row_id = "`order_detail`.`id_order_detail`";
                                    $product_quantity = "`order_detail`.`product_quantity`";
                                    if ($attribute === "product_quantity") {
                                        $sum_field = 1;
                                    } else {
                                        $sum_field = "`" . pSQL($array_tables[$key]) . "`.`" . pSQL($attribute) . "`";
                                    }

                                    $this->select(
                                        "GROUP_CONCAT(DISTINCT CONCAT(
                                            " . $row_id . ", 
                                            ':', 
                                            (" . $product_quantity . " * " . $sum_field . ")
                                        ) SEPARATOR ';') AS '" . $attribute_as . "'"
                                    );
                                } else {
                                    $default_select = true;
                                }
                                break;
                        }
                        break;

//                    case 'customer':
//                        switch ($attribute_as) {
//                            case '':
//
//                                break;
//                            default:
//                                $default_select = true;
//                                break;
//                        }
//                        break;

                    case 'cart':
                        switch ($attribute_as) {
                            case 'cartstatus':
                                $this->select("IF (IFNULL(o.id_order, 'Non ordered') = 'Non ordered',
                                    IF(TIME_TO_SEC(
                                    TIMEDIFF('" . pSQL(date('Y-m-d H:i:00', time())) . "', a.`date_add`)) > 86400,
                                    '" . $this->context->getTranslator()->trans('Abandoned cart') . "',
                                    '" . $this->context->getTranslator()->trans('Non ordered') . "'),
                                    o.id_order) AS `" . $attribute_as . "`");
                                break;
                            default:
                                $default_select = true;
                                break;
                        }
                        break;
                    default:
                        $default_select = true;
                        break;
                }
            } elseif ($array_tables[$key] == 'extracolumns') {
                switch ($tab) {
                    case 'customer':
                        switch ($attribute_as) {
                            case 'extracolumnsnb_orders':
                                if (!in_array('orders', $this->already_join)) {
                                    $on = "`" . $this->getAlias('customer') . "`.`id_customer` =
                                        `orders`.`id_customer`";
                                    $this->joinQuery(
                                        'left',
                                        $this->getAlias('customer'),
                                        'orders',
                                        'orders',
                                        $on
                                    );
                                    $this->already_join[] = 'orders';
                                }
                                $this->select("COUNT(DISTINCT `orders`.`reference`) AS '" . pSQL($attribute_as) . "'");
                                break;
                            case 'extracolumnsdefault_group':
                                if (!in_array('default_group', $this->already_join)) {
                                    $on = "`" . $this->getAlias('customer') . "`.`id_default_group` =
                                        `default_group`.`id_group`
                                        AND `default_group`.`id_lang` = '" . (int)$this->context->language->id . "'";
                                    $this->joinQuery(
                                        'left',
                                        $this->getAlias('customer'),
                                        'group_lang',
                                        'default_group',
                                        $on
                                    );
                                    $this->already_join[] = 'default_group';
                                }
                                $array_tables[$key] = 'default_group';
                                $attribute = 'name';
                                $default_select = true;
                                break;
                        }
                        break;

                    case 'orders':
                        switch ($attribute_as) {
                            case 'extracolumnsdefault_group':
                                if (!in_array('default_group', $this->already_join)) {
                                    $on = "`" . $this->getAlias('customer') . "`.`id_default_group` =
                                        `default_group`.`id_group`
                                        AND `default_group`.`id_lang` = '" . (int)$this->context->language->id . "'";
                                    $this->joinQuery(
                                        'left',
                                        $this->getAlias('customer'),
                                        'group_lang',
                                        'default_group',
                                        $on
                                    );
                                    $this->already_join[] = 'default_group';
                                }
                                $array_tables[$key] = 'default_group';
                                $attribute = 'name';
                                $default_select = true;
                                break;
                            case 'extracolumnssum_discount':
                                if (!in_array('order_cart_rule', $this->already_join)) {
                                    $on = "`" . $this->getAlias('orders') . "`.`id_order` =
                                        `order_cart_rule`.`id_order`";
                                    if (version_compare(_PS_VERSION_, '1.7.7.0', '>=')) {
                                        $on .= " AND `order_cart_rule`.`deleted` = 0";
                                    }
                                    $this->joinQuery(
                                        'left',
                                        $this->getAlias('orders'),
                                        'order_cart_rule',
                                        'order_cart_rule',
                                        $on
                                    );
                                    $this->already_join[] = 'order_cart_rule';
                                }

                                $this->select(
                                    "GROUP_CONCAT(DISTINCT CONCAT(
                                            `id_order_cart_rule`, 
                                            ':', 
                                            (`order_cart_rule`.`value`)
                                        ) SEPARATOR ';') AS '" . pSQL($attribute_as) . "'"
                                );
                                break;
                            case 'extracolumnsinvoice_name':
                                $this->select(
                                    "`" . $this->getAlias('orders') . "`.`invoice_number` AS " . pSQL($attribute_as)
                                );
                                break;
                            case 'extracolumnscustomer':
                                $this->select(
                                    "CONCAT(
                                        LEFT(`" . $this->getAlias('customer') . "`.`firstname`, 1),
                                        '. ',
                                        `" . $this->getAlias('customer') . "`.`lastname`
                                        ) AS " . pSQL($attribute_as)
                                );
                                break;
                            case 'extracolumnstotal_spent':
                                $query = new DbQuery();
                                $shops = implode(',', Shop::getContextListShopID());
                                $query->select("SUM(`total_paid_real` / `total_spent_o`.`conversion_rate`)")
                                    ->from('orders', 'total_spent_o')
                                    ->where(
                                        '`total_spent_o`.`id_customer` = `'.$this->getAlias('orders').'`.`id_customer`'
                                    )
                                    ->where('`total_spent_o`.`valid` = 1')
                                    ->where('`total_spent_o`.`id_shop` IN (' . pSQL($shops) . ')');

                                $this->select("(" . $query->build() . ") AS '" . $attribute_as . "'");
                                break;
                        }
                        break;
                }
            }

            if ($default_select) {
                $select_type = "group_concat";
                switch ($tab) {
                    case 'orders':
                        switch ($config_tables[$key]) {
                            case 'orders':
                            case 'customer':
                                $select_type = "basic";
                                break;
                        }
                        break;
                    case 'customer':
                        switch ($array_tables[$key]) {
                            case 'customer':
                                $select_type = "basic";
                                break;
                        }
                        break;
                    case 'address':
                        switch ($array_tables[$key]) {
                            case 'address':
                                $select_type = "basic";
                                break;
                        }
                        break;
                    case 'cart':
                        switch ($array_tables[$key]) {
                            case 'cart':
                                $select_type = "basic";
                                break;
                        }
                        break;
                    case 'customer_thread':
                        switch ($array_tables[$key]) {
                            case 'customer_thread':
                                $select_type = "basic";
                                break;
                        }
                        break;
                }

                if ($select_type == "basic") {
                    $this->select(
                        "`" . pSQL($array_tables[$key]) . "`.`" . pSQL($attribute) . "` AS '" . $attribute_as . "'"
                    );
                } else {
                    $this->select(
                        "GROUP_CONCAT(
                            DISTINCT `" . pSQL($array_tables[$key]) . "`.`" . pSQL($attribute) . "`
                            SEPARATOR '; ') AS '" . $attribute_as . "'"
                    );
                }
            }
        }
    }

    public function getSelect()
    {
        return $this->query['select'];
    }

    public function getJoin()
    {
        return $this->query['join'];
    }

    public function joinQuery($type, $fromAlias, $join, $alias, $condition = null)
    {
        if ($this->use_parent) {
            $callback = $type . 'Join';
            parent::$callback($join, $alias, $condition);
        } else {
            $this->query['join'][] = array(
                'type' => $type,
                'fromAlias' => $fromAlias,
                'join' => $join,
                'alias' => $alias,
                'condition' => $condition
            );
        }
        return $this;
    }

    private function cbAttributeAs($column)
    {
        return $this->getAttributeAs($column['table'], $column['attribute']);
    }

    /**
     * @param string $tab
     * @param mixed $search_criteria
     * @param \Doctrine\ORM\QueryBuilder $query
     */
    public function setHaving($tab, $search_criteria, $query)
    {
        $this->setParameters($tab);
        $filters = $search_criteria->getFilters();
        $orderBy = $search_criteria->getOrderBy();

        //TOCHANGE
        switch ($tab) {
            case 'orders':
                $default_fields = array(
                    'id_order',
                    'new',
                    'country_name',
                    'total_paid_tax_incl',
                    'osname',
                    'reference',
                    'company',
                    'payment',
                    'customer',
                    'date_add'
                );
                $strictComparisonFilters = array(
                    'ordersid_order' => array(
                        'table' => $this->getAlias('orders'),
                        'attribute' => 'id_order'
                    ),
                    'country_langname' => array(
                        'table' => $this->getAlias('country_lang'),
                        'attribute' => 'id_country'
                    ),
                    'orderscurrent_state' => array(
                        'table' => $this->getAlias('orders'),
                        'attribute' => 'current_state'
                    ),
                );

                $field_names = array(
                    // 'extracolumnscustomer' => 'customer',
                    'extracolumnsnew_client' => 'new',
//                    'ordersdate_add' => 'o.`date_add`'
                );
                break;

            case 'customer':
                $default_fields = array(
                    'id_customer',
                    'social_title',
                    'firstname',
                    'lastname',
                    'email',
                    'active',
                    'newsletter',
                    'optin',
                    'date_add',
                    'company'
                );
                $strictComparisonFilters = array(
                    'customerid_customer' => array(
                        'table' => $this->getAlias('customer'),
                        'attribute' => 'id_customer'
                    ),
                    'gender_langname' => array(
                        'table' => $this->getAlias('gender_lang'),
                        'attribute' => 'id_gender'
                    )
                );

                $field_names = array();
                break;

            case 'address':
                $default_fields = array(
                    'id_address',
                    'firstname',
                    'lastname',
                    'address1',
                    'postcode',
                    'city',
                    'id_country'
                );
                $strictComparisonFilters = array(
                    'addressid_address' => array(
                        'table' => $this->getAlias('address'),
                        'attribute' => 'id_address'
                    )
                );

                $field_names = array();
                break;

            default:
                $default_fields = array();
                $strictComparisonFilters = array();

                $field_names = array();
                break;
        }

        $columns = $this->getColumns($tab);
        $likeComparisonFilters = array();
        $dateComparisonFilters = array();
        $priceComparisonFilters = array();

        if ($this->replace === true) {
            $columns[] = array(
                'attribute' => 'id_order',
                'table' => 'orders',
                'type' => 'integer'
            );
            $attributes_as = array_map(array($this, 'cbAttributeAs'), $columns);
            foreach (array_keys($filters) as $filter_key) {
                switch ($filter_key) {
                    case 'id_order':
                        $filters['ordersid_order'] = $filters[$filter_key];
                        break;
                    case 'id_customer':
                        $filters['customerid_customer'] = $filters[$filter_key];
                        break;
                    case 'id_address':
                        $filters['addressid_address'] = $filters[$filter_key];
                        break;
                }
                if (!in_array($filter_key, $attributes_as)) {
                    unset($filters[$filter_key]);
                }
            }

            $default_fields = [];
            $query
                ->orWhere('1');

            switch ($tab) {
                case 'orders':
                    $query->andWhere('o.`id_shop` IN (:context_shop_ids)');
                    break;
                case 'customer':
                    $query->andWhere('c.id_shop IN (:context_shop_ids)');
                    break;
                case 'address':
                    $query
                        ->where('a.`id_customer` != 0')
                        ->andWhere('a.`deleted` = 0')
                        ->andWhere('customer.id_shop IN (:context_shop_ids)');
                    break;
            }
        }

        foreach ($columns as $column) {
//            $field_name = $column['table'] . $column['attribute'];
            $field_name = $this->getAttributeAs($column['table'], $column['attribute']);

            if ($field_name == $orderBy && !in_array($orderBy, $default_fields)) {
                $query->orderBy($orderBy, $search_criteria->getOrderWay());
            }
            if (($column['type'] == 'date' || $column['type'] == 'datetime') && $column['table'] != 'extracolumns') {
                $field_names[$field_name] = $this->getAlias($column['table']) . '.`' . pSQL($column['attribute']) . '`';
            }

            if (in_array($field_name, array_merge(array_keys($strictComparisonFilters), $default_fields))) {
                continue;
            }

            switch ($column['type']) {
                case 'date':
                case 'datetime':
                    $dateComparisonFilters[] = $field_name;
                    break;
                case 'price':
                    $priceComparisonFilters[] = $field_name;
                    break;
                default:
                    $likeComparisonFilters[] = $field_name;
                    break;
            }
        }

        foreach ($filters as $name => $value) {
            $field_name = array_key_exists($name, $field_names) ? $field_names[$name] : $name;

            if (in_array($name, array_keys($strictComparisonFilters))) {
                $query->andWhere(
                    "`" . pSQL($strictComparisonFilters[$name]['table']) . "`.
                    `" . pSQL($strictComparisonFilters[$name]['attribute']) . "` = '" . pSQL($value) . "'"
                );
                // $query->andHaving("`" . pSQL($field_name) . "` = '" . pSQL($value) . "'");
                continue;
            }

            if (in_array($name, $likeComparisonFilters)) {
                $query->andHaving("`" . pSQL($field_name) . "` LIKE '%" . pSQL($value) . "%'");
                continue;
            }

            if (in_array($name, $priceComparisonFilters)) {
                $value = (float)preg_replace('/(\d+)[.,]?(\d*)/', '$1.$2', $value);
                $query->andHaving("`" . pSQL($field_name) . "` LIKE '" . pSQL($value) . "%'");
                continue;
            }

            if (in_array($name, $dateComparisonFilters)) {
                if (isset($value['from'])) {
                    $query->andWhere($field_name . " >= '" . pSQL($value['from']) . " 0:0:0'");
                }
                if (isset($value['to'])) {
                    $query->andWhere($field_name . " <= '" . pSQL($value['to']) . " 23:59:59'");
                }
                continue;
            }
        }
    }

    public function getFieldsBefore177($tab)
    {
        $get_configs = $this->getConfigurations($tab);

        if (empty($get_configs)) {
            return 0;
        }

        $this->getParameters($tab);
        $array_attributes = $get_configs['array_attributes'];
        $config_tables = $get_configs['config_tables'];
        $array_titles = $get_configs['array_titles'];
        $array_types = $get_configs['array_types'];
        $array_tables = $get_configs['array_tables'];

        $fields_to_display = array();

        switch ($tab) {
            case 'orders':
                $fields_to_display['id_order'] = array(
                    'title' => 'ID',
                    'align' => 'text-center',
                    'class' => 'fixed-width-xs',
                    'filter_key' => 'a!id_order'
                );
                break;

            case 'customer':
                $fields_to_display['id_customer'] = array(
                    'title' => 'ID',
                    'align' => 'text-center',
                    'class' => 'fixed-width-xs',
                    'filter_key' => 'a!id_customer'
                );
                break;

            case 'address':
                $fields_to_display['id_address'] = array(
                    'title' => 'ID',
                    'align' => 'text-center',
                    'class' => 'fixed-width-xs',
                    'filter_key' => 'a!id_address'
                );
                break;

            case 'cart':
                $fields_to_display['id_cart'] = array(
                    'title' => 'ID',
                    'align' => 'text-center',
                    'class' => 'fixed-width-xs',
//                    'filter_key' => 'a!id_cart'
                );
                break;

            case 'customer_thread':
                $fields_to_display['id_customer_thread'] = array(
                    'title' => 'ID',
                    'align' => 'text-center',
                    'class' => 'fixed-width-xs',
                    'filter_key' => 'a!id_customer_thread'
                );
                break;
        }

        foreach ($array_attributes as $key => $attribute) {
            if (!array_key_exists($key, $config_tables) || empty($config_tables[$key]) || empty($attribute)) {
                continue;
            }

            $attribute_as = $this->getAttributeAs($config_tables[$key], $attribute);

            $add_filter_key = $config_tables[$key] != 'extracolumns';

            switch ($tab) {
                case 'orders':
                    switch ($attribute_as) {
                        case 'order_state_langname':
                            $fields_to_display[$attribute_as]['color'] = 'color';
                            break;
                        case 'orderstotal_paid_tax_incl':
                            $fields_to_display[$attribute_as]['badge_success'] = true;
                            break;
                        case 'customer':
                            $array_types[$key] = false;
                            $add_filter_key = false;
                            break;
                        case 'new':
                            $array_types[$key] = false;
                            $fields_to_display[$attribute_as]['search'] = false;
                            $fields_to_display[$attribute_as]['orderby'] = false;
                            $fields_to_display[$attribute_as]['type'] = 'bool';
                            $add_filter_key = false;
                            break;
                        case 'imageid_image':
                            $fields_to_display[$attribute_as]['callback'] = 'printProductsThumbnails';
                            $fields_to_display[$attribute_as]['callback_object'] = $this;
                            $fields_to_display[$attribute_as]['orderby'] = false;
                            $fields_to_display[$attribute_as]['search'] = false;
                            $fields_to_display[$attribute_as]['class'] = 'ec-thumbnails-td';
                            $array_types[$key] = false;
                            $add_filter_key = false;
                            break;
                        case 'order_carriertracking_number':
                            $fields_to_display[$attribute_as] = array(
                                'filter_key' => $array_tables[$key] . '!' . $attribute,
                                'callback' => 'printTrackingNumberUrl',
                                'callback_object' => $this,
                                'remove_onclick' => true
                            );
                            break;
                        case 'extracolumnsinvoice_name':
                            $array_types[$key] = false;
                            $fields_to_display[$attribute_as]['search'] = false;
                            $fields_to_display[$attribute_as]['orderby'] = false;

                            $fields_to_display[$attribute_as]['callback'] = 'getInvoiceNumber';
                            $fields_to_display[$attribute_as]['callback_object'] = new Order();
                            break;
                        case 'order_detailproduct_quantity':
                            $fields_to_display[$attribute_as]['search'] = false;
                            $fields_to_display[$attribute_as]['orderby'] = false;
                            $fields_to_display[$attribute_as]['callback'] = 'printSumInteger';
                            $fields_to_display[$attribute_as]['callback_object'] = $this;
                            break;
                        case 'order_detailproduct_weight':
                        case 'extracolumnssum_discount':
                            $fields_to_display[$attribute_as]['search'] = false;
                            $fields_to_display[$attribute_as]['orderby'] = false;
                            $fields_to_display[$attribute_as]['callback'] = 'printSumDecimal';
                            $fields_to_display[$attribute_as]['callback_object'] = $this;
                            break;
                        case 'extracolumnstotal_spent':
                        case 'total_spent':
                            $fields_to_display[$attribute_as]['search'] = false;
                            $fields_to_display[$attribute_as]['havingFilter'] = true;
                            $add_filter_key = false;
                            break;
                    }
                    break;

                case 'customer':
                    switch ($attribute_as) {
                        case 'connect':
                            $fields_to_display[$attribute_as]['search'] = false;
                            $fields_to_display[$attribute_as]['havingFilter'] = true;
                            $add_filter_key = false;
                            break;
                        case 'total_spent':
                            $fields_to_display[$attribute_as]['search'] = false;
                            $fields_to_display[$attribute_as]['badge_success'] = true;
                            $fields_to_display[$attribute_as]['havingFilter'] = true;
                            $add_filter_key = false;
                            break;
                        case 'customernewsletter':
                            $fields_to_display[$attribute_as]['type'] = 'bool';
                            $fields_to_display[$attribute_as]['callback'] = 'printNewsIcon';
                            $fields_to_display[$attribute_as]['orderby'] = false;
                            $array_types[$key] = false;
                            break;
                        case 'customeroptin':
                            $fields_to_display[$attribute_as]['type'] = 'bool';
                            $fields_to_display[$attribute_as]['callback'] = 'printOptinIcon';
                            $fields_to_display[$attribute_as]['orderby'] = false;
                            $array_types[$key] = false;
                            break;
                        case 'customeractive':
                            $fields_to_display[$attribute_as]['type'] = 'bool';
                            $fields_to_display[$attribute_as]['active'] = 'status';
                            $fields_to_display[$attribute_as]['orderby'] = false;
                            $array_types[$key] = false;
                            break;
                    }
                    break;

                case 'cart':
                    switch ($attribute_as) {
                        case 'carttotal':
                            $fields_to_display[$attribute_as] =
                                array(
                                    'title' => !empty($array_titles[$key]) ? $array_titles[$key] : $attribute,
                                    'align' => 'text-center',
                                    'callback' => 'getOrderTotalUsingTaxCalculationMethod',
                                    'badge_success' => true,
                                    'orderBy' => false,
                                    'search' => false,
                                    'filter_key' => 'a!id_cart'
                                );
                            continue 3;
                        case 'carriername':
                            $fields_to_display[$attribute_as]['callback'] = 'replaceZeroByShopName';
                            break;
                        case 'cartstatus':
                            $fields_to_display[$attribute_as] =
                                array(
                                    'align' => 'text-center',
                                    'badge_danger' => true,
                                    'havingFilter' => true
                                );
                            break;
                    }
                    break;

                case 'customer_thread':
                    switch ($attribute_as) {
                        case 'customer_threadstatus':
                            $fields_to_display[$attribute_as]['icon'] = array(
                                'open' => array(
                                    'class' => 'icon-circle text-success',
                                    'alt' => $this->context->getTranslator()->trans('Open'),
                                ),
                                'closed' => array(
                                    'class' => 'icon-circle text-danger',
                                    'alt' => $this->context->getTranslator()->trans('Closed'),
                                ),
                                'pending1' => array(
                                    'class' => 'icon-circle text-warning',
                                    'alt' => $this->context->getTranslator()->trans('Pending') . ' 1',
                                ),
                                'pending2' => array(
                                    'class' => 'icon-circle text-warning',
                                    'alt' => $this->context->getTranslator()->trans('Pending') . ' 2',
                                ),
                            );
                            break;
                    }
                    break;
            }

            $fields_to_display[$attribute_as]['title'] =
                !empty($array_titles[$key]) ? $array_titles[$key] : $attribute;
            $fields_to_display[$attribute_as]['align'] = 'text-center';

            if (array_key_exists($key, $array_types) && !empty($array_types[$key])) {
                switch ($array_types[$key]) {
                    case 'boolean':
                        $data_type = Db::getInstance()->getValue(
                            "SELECT `data_type`
                            FROM `information_schema`.`COLUMNS`
                            WHERE `table_schema` = '" . _DB_NAME_ . "'
                            AND `table_name` = '" . _DB_PREFIX_ . pSQL($config_tables[$key]) . "'
                            AND `column_name` = '" . pSQL($attribute) . "'"
                        );

                        if ($data_type == "tinyint") {
                            $fields_to_display[$attribute_as]['type'] = 'bool';
                            if (version_compare(_PS_VERSION_, '1.7.0.0', '<')) {
                                $fields_to_display[$attribute_as]['callback'] = 'printBoolean';
                                $fields_to_display[$attribute_as]['callback_object'] = $this;
                            }
                        }
                        break;
                    case 'price':
                        $fields_to_display[$attribute_as]['type'] = 'price';
                        break;
                    case 'percent':
                        $fields_to_display[$attribute_as]['type'] = 'percent';
                        break;
                    case 'date':
                        $fields_to_display[$attribute_as]['type'] = 'date';
                        break;
                    case 'datetime':
                        $fields_to_display[$attribute_as]['type'] = 'datetime';
                        break;
                    case 'select':
                        if (array_key_exists($attribute_as, $this->select_fields)) {
                            $select_values = Db::getInstance()->executeS(
                                "SELECT DISTINCT `a`.`" . $this->select_fields[$attribute_as]['id'] . "` AS 'id',
                                `a`.`" . $this->select_fields[$attribute_as]['name'] . "` AS `name`
                                FROM `" . _DB_PREFIX_ . $this->select_fields[$attribute_as]['table'] . "` `a`" .
                                (isset($this->select_fields[$attribute_as]['join']) ?
                                    $this->select_fields[$attribute_as]['join'] : "") .
                                ($this->select_fields[$attribute_as]['lang'] ?
                                    " WHERE `a`.`id_lang` = '" . (int)$this->context->language->id . "'" : "") .
                                " ORDER BY `name` ASC"
                            );
                            $select_display = array();

                            foreach ($select_values as $select_value) {
                                $select_display[$select_value['id']] = $select_value['name'];
                            }

                            $fields_to_display[$attribute_as]['type'] = 'select';
                            $fields_to_display[$attribute_as]['list'] = $select_display;
                            $fields_to_display[$attribute_as]['filter_key'] =
                                (isset($this->select_fields[$attribute_as]['filter_key']) ?
                                    $this->select_fields[$attribute_as]['filter_key'] :
                                    $array_tables[$key]) . '!' . $this->select_fields[$attribute_as]['id'];
                            continue 2;
                        }
                        break;
                }
            }

            if ((!array_key_exists('type', $fields_to_display[$attribute_as]) && $tab != 'cart') ||
                !(array_key_exists('search', $fields_to_display[$attribute_as]) &&
                    $config_tables[$key] == 'extracolumns')) {
                $fields_to_display[$attribute_as]['havingFilter'] = true;
                $add_filter_key = false;
            }

            if (!(array_key_exists('search', $fields_to_display[$attribute_as]) &&
                    $fields_to_display[$attribute_as]['search']) && $add_filter_key) {
                $fields_to_display[$attribute_as]['filter_key'] = $array_tables[$key] . '!' . $attribute;
            }
        }

        return $fields_to_display;
    }

    /**
     * @param $tab
     * @return array
     */
    private function setParameters($tab)
    {
        $parameters = Db::getInstance()->getRow(
            "SELECT IF(
                `ep`.`id_extracolumns_profiles` <> 0 OR `ep`.`id_extracolumns_profiles` IS NULL,
                IF(
                    `ep`.`id_extracolumns_profiles` = 1 OR `ep`.`id_extracolumns_profiles` IS NULL,
                    `a`.`replace`,
                    `p`.`replace`
                ),
                0
            ) AS 'replace', IF(
                `ep`.`id_extracolumns_profiles` <> 0 OR `ep`.`id_extracolumns_profiles` IS NULL,
                IF(
                    `ep`.`id_extracolumns_profiles` = 1 OR `ep`.`id_extracolumns_profiles` IS NULL,
                    `a`.`start`,
                    `p`.`start`
                ),
                0
            ) AS 'start'
            FROM `" . _DB_PREFIX_ . "extracolumns` `a`
            LEFT JOIN `" . _DB_PREFIX_ . "extracolumns_employee_profile` `ep`
                ON `a`.`id_extracolumns` = `ep`.`id_extracolumns`
                    AND `ep`.`id_employee` = " . (int)Context::getContext()->employee->id . "
            LEFT JOIN `" . _DB_PREFIX_ . "extracolumns_profiles` `p`
                ON `ep`.`id_extracolumns_profiles` = `p`.`id_extracolumns_profiles`
            WHERE `a`.`tab` = '" . pSQL($tab) . "'"
        );

        $this->replace = (bool)$parameters['replace'];

        return $parameters;
    }

    /**
     * @param string $tab
     * @param \PrestaShop\PrestaShop\Core\Grid\Definition\GridDefinition $definition
     */
    public function getFields($tab, $controller, $definition)
    {
        $get_configs = $this->getConfigurations($tab);

        if (empty($get_configs)) {
            return 0;
        }

        $this->getParameters($tab);
        $array_attributes = $get_configs['array_attributes'];
        $config_tables = $get_configs['config_tables'];
        $array_titles = $get_configs['array_titles'];
        $array_types = $get_configs['array_types'];

        $parameters = $this->setParameters($tab);

        $columns = $definition->getColumns();
        $filters = $definition->getFilters();
        $columns_array = $columns->toArray();

        if ($parameters['replace']) {
            // TOCHANGE
            $default_columns = array(
                'orders' => array(
                    'id_order',
                    'orders_bulk',
                    'actions'
                ),
                'address' => array(
                    'id_address',
                    'addresses_bulk',
                    'actions'
                ),
                'customer' => array(
                    'id_customer',
                    'customers_bulk',
                    'actions'
                )
            );
            // TOCHANGE
            $associated_columns = array(
                'orders' => array(),
                'address' => array(
                    'country_name' => 'id_country'
                ),
                'customer' => array()
            );

            foreach ($columns->toArray() as $column) {
                if (!in_array($column['id'], $default_columns[$tab])) {
                    $columns->remove($column['id']);
                    $to_remove = (array_key_exists($column['id'], $associated_columns[$tab]) ?
                        $associated_columns[$tab][$column['id']] :
                        $column['id']);
                    $filters->remove($to_remove);
                }
            }
        } else {
//            $filters->remove('new');
        }

        foreach ($array_attributes as $key => $attribute) {
            if (!array_key_exists($key, $config_tables) || empty($config_tables[$key]) || empty($attribute)) {
                continue;
            }

            $attribute_as = $this->getAttributeAs($config_tables[$key], $attribute);
            $column_options = array();
            $filter_options = array();
            $no_filter = false;

            switch ($tab) {
                case 'orders':
                    switch ($attribute_as) {
                        case 'orderscurrent_state':
                            $orderStatesChoiceProvider =
                                $controller->get('prestashop.core.form.choice_provider.order_state_by_id');

                            $column_field = new PrestaShop\PrestaShop\Core\Grid\Column\Type\Common\ChoiceColumn(
                                $attribute_as
                            );
                            $column_options = array_merge(
                                $column_options,
                                array(
                                    'route' => 'admin_orders_list_update_status',
                                    'color_field' => 'color',
                                    'choice_provider' => $orderStatesChoiceProvider,
                                    'record_route_params' => [
                                        'id_order' => 'orderId',
                                    ],
                                )
                            );

                            $filter_type = Symfony\Component\Form\Extension\Core\Type\ChoiceType::class;
                            $filter_options = array_merge(
                                $filter_options,
                                array(
                                    'choices' => $orderStatesChoiceProvider->getChoices(),
                                    'translation_domain' => false,
                                )
                            );
                            break;
                        case 'orderstotal_paid_tax_incl':
                            $column_field = new PrestaShop\PrestaShop\Core\Grid\Column\Type\OrderPriceColumn(
                                $attribute_as
                            );
                            $column_options = array_merge(
                                $column_options,
                                array(
                                    'is_paid_field' => 'paid',
                                    'clickable' => true,
                                )
                            );
                            break;
                        case 'extracolumnsnew_client':
                            $column_field = new PrestaShop\PrestaShop\Core\Grid\Column\Type\BooleanColumn(
                                $attribute_as
                            );
                            $column_options = array_merge(
                                $column_options,
                                array(
                                    'true_name' => $this->context->getTranslator()->trans('Yes', [], 'Admin.Global'),
                                    'false_name' => $this->context->getTranslator()->trans('No', [], 'Admin.Global'),
                                    'field' => 'new'
                                )
                            );

                            $no_filter = true;
                            $array_types[$key] = false;
                            break;
                        case 'extracolumnscustomer':
                            $column_field = new PrestaShop\PrestaShop\Core\Grid\Column\Type\DisableableLinkColumn(
                                $attribute_as
                            );

                            $column_options = array_merge(
                                $column_options,
                                array(
                                    'field' => 'extracolumnscustomer',
                                    'disabled_field' => 'deleted_customer',
                                    'route' => 'admin_customers_view',
                                    'route_param_name' => 'customerId',
                                    'route_param_field' => 'id_customer',
                                    'target' => '_blank',
                                )
                            );
                            break;
                        case 'extracolumnstotal_spent':
                            $column_field = new PrestaShop\PrestaShop\Core\Grid\Column\Type\Common\BadgeColumn(
                                $attribute_as
                            );

                            $column_options = array_merge(
                                $column_options,
                                array(
                                    'field' => 'extracolumnstotal_spent',
                                    'empty_value' => '--',
                                )
                            );
                            $no_filter = true;
                            break;
                        case 'imageid_image':
                            $column_field = new PrestaShop\Module\ExtraColumns\Grid\Column\ThumbnailsColumn(
                                $attribute_as
                            );

                            $column_options = array_merge(
                                $column_options,
                                array(
                                    'field' => $attribute_as,
                                    'image_size' => 40,
                                )
                            );
                            $no_filter = true;
                            break;
//                        case 'extracolumnsinvoice_name':
//
//                            break;
                    }
                    break;

                case 'customer':
                    switch ($attribute_as) {
//                        case 'gender_langname':
//                            $genderChoices = $controller->get('prestashop.adapter.form.choice_provider.gender_by_id_choice_provider')->getChoices();
//
//                            $filter_type = Symfony\Component\Form\Extension\Core\Type\ChoiceType::class;
//                            $filter_options = array_merge(
//                                $filter_options,
//                                array(
//                                    'choices' => $genderChoices,
//                                    'expanded' => false,
//                                    'multiple' => false,
//                                    'choice_translation_domain' => false,
//                                )
//                            );
//                            break;
                        case 'customeractive':
                            $column_field = new PrestaShop\PrestaShop\Core\Grid\Column\Type\Common\ToggleColumn(
                                $attribute_as
                            );

                            $column_options = array_merge(
                                $column_options,
                                array(
                                    'primary_field' => 'id_customer',
                                    'route' => 'admin_customers_toggle_status',
                                    'route_param_name' => 'customerId',
                                )
                            );

                            $filter_type = PrestaShopBundle\Form\Admin\Type\YesAndNoChoiceType::class;
                            break;
                        case 'customernewsletter':
                            $column_field = new PrestaShop\PrestaShop\Core\Grid\Column\Type\Common\ToggleColumn(
                                $attribute_as
                            );

                            $column_options = array_merge(
                                $column_options,
                                array(
                                    'primary_field' => 'id_customer',
                                    'route' => 'admin_customers_toggle_newsletter_subscription',
                                    'route_param_name' => 'customerId',
                                )
                            );
                            break;
                        case 'customeroptin':
                            $column_field = new PrestaShop\PrestaShop\Core\Grid\Column\Type\Common\ToggleColumn(
                                $attribute_as
                            );

                            $column_options = array_merge(
                                $column_options,
                                array(
                                    'primary_field' => 'id_customer',
                                    'route' => 'admin_customers_toggle_partner_offer_subscription',
                                    'route_param_name' => 'customerId',
                                )
                            );
                            break;
                        case 'extracolumnslast_visit':
                            $column_options = array_merge(
                                $column_options,
                                array(
                                    'field' => 'connect',
                                )
                            );
                            $no_filter = true;
                            break;
                        case 'extracolumnstotal_spent':
                            $column_field = new PrestaShop\PrestaShop\Core\Grid\Column\Type\Common\BadgeColumn(
                                $attribute_as
                            );

                            $column_options = array_merge(
                                $column_options,
                                array(
                                    'field' => 'total_spent',
                                    'empty_value' => '--',
                                )
                            );
                            $no_filter = true;
                            break;
                    }
                    break;
            }

            if (array_key_exists($key, $array_types) && !empty($array_types[$key]) && !isset($column_field)) {
                $data_type = Db::getInstance()->getValue(
                    "SELECT `data_type`
                            FROM `information_schema`.`COLUMNS`
                            WHERE `table_schema` = '" . _DB_NAME_ . "'
                            AND `table_name` = '" . _DB_PREFIX_ . pSQL($config_tables[$key]) . "'
                            AND `column_name` = '" . pSQL($attribute) . "'"
                );

                switch ($array_types[$key]) {
                    case 'boolean':
                        if ($data_type == "tinyint") {
                            // TODO action
                            $column_field = new PrestaShop\PrestaShop\Core\Grid\Column\Type\BooleanColumn(
                                $attribute_as
                            );
                            $column_options = array_merge(
                                $column_options,
                                array(
                                    'true_name' => $this->context->getTranslator()->trans('Yes', [], 'Admin.Global'),
                                    'false_name' => $this->context->getTranslator()->trans('No', [], 'Admin.Global')
                                )
                            );

                            $filter_type = PrestaShopBundle\Form\Admin\Type\YesAndNoChoiceType::class;
                        }
                        break;
                    case 'date':
                    case 'datetime':
                        $format = $array_types[$key] == 'date' ?
                            $this->context->language->date_format_lite :
                            $this->context->language->date_format_full;

                        if ($data_type == 'datetime' || $data_type == 'timestamp' || $data_type == 'date') {
                            $column_field = new PrestaShop\Module\ExtraColumns\Grid\Column\DateTimeColumn(
                                $attribute_as
                            );

                            $column_options = array_merge(
                                $column_options,
                                array(
                                    'format' => $format
                                )
                            );

                            $filter_type = PrestaShopBundle\Form\Admin\Type\DateRangeType::class;
                        }
                        break;
                    case 'select':
                        if (array_key_exists($attribute_as, $this->select_fields)) {
                            $select_values = Db::getInstance()->executeS(
                                "SELECT DISTINCT `a`.`" . $this->select_fields[$attribute_as]['id'] . "` AS 'id',
                                `a`.`" . $this->select_fields[$attribute_as]['name'] . "` AS `name`
                                FROM `" . _DB_PREFIX_ . $this->select_fields[$attribute_as]['table'] . "` `a`" .
                                (isset($this->select_fields[$attribute_as]['join']) ?
                                    $this->select_fields[$attribute_as]['join'] : "") .
                                ($this->select_fields[$attribute_as]['lang'] ?
                                    " WHERE `a`.`id_lang` = '" . (int)$this->context->language->id . "'" : "") .
                                " ORDER BY `name` ASC"
                            );
                            $select_display = array();

                            foreach ($select_values as $select_value) {
                                $select_display[$select_value['name']] = $select_value['id'];
                            }

                            $filter_type = Symfony\Component\Form\Extension\Core\Type\ChoiceType::class;
                            $filter_options = array_merge(
                                $filter_options,
                                array(
                                    'choices' => $select_display,
                                    'translation_domain' => false,
                                )
                            );
                        }
                        break;
//                    case 'percent':
//                        $column_field = new PrestaShop\Module\ExtraColumns\Grid\Column\PercentColumn(
//                            $attribute_as
//                        );
//                        break;
                }
            }

            $title = !empty($array_titles[$key]) ? $array_titles[$key] : $attribute;

            if (!isset($column_field)) {
                $column_field = new PrestaShop\PrestaShop\Core\Grid\Column\Type\DataColumn($attribute_as);
            }

            $column_field->setName($title)
                ->setOptions(array_merge(
                    array(
                        'field' => $attribute_as
                    ),
                    $column_options
                ));

            if (!$parameters['replace'] && $parameters['start'] == 'id' && array_key_exists(2, $columns_array)) {
                $columns->addBefore($columns_array[2]['id'], $column_field);
            } else {
                $columns->addBefore('actions', $column_field);
            }

            if (!$no_filter) {
                if (!isset($filter_type)) {
                    $filter_type = Symfony\Component\Form\Extension\Core\Type\TextType::class;
                }

                $filters->add(
                    (new PrestaShop\PrestaShop\Core\Grid\Filter\Filter($attribute_as, $filter_type))->setTypeOptions(
                        array_merge(
                            array(
                                'required' => false,
                                'attr' => array(
                                    'placeholder' => $title,
                                ),
                            ),
                            $filter_options
                        )
                    )->setAssociatedColumn($attribute_as)
                );
                unset($filter_type);
            }
            unset($column_field);
            unset($column_options);
            unset($filter_options);
        }

        $templates = array_merge(
            array(
                array(
                    'id_extracolumns_profiles' => 0,
                    'name' => $controller->l('Default')
                ),
                array(
                    'id_extracolumns_profiles' => 1,
                    'name' => $controller->l('Current config')
                )
            ),
            ExtraColumnsProfile::getProfilesByTabName($tab)
        );
        $actions = $definition->getGridActions();

        $actions->add(
            (new PrestaShop\Module\ExtraColumns\Grid\Action\TemplatesGridAction('ec_templates'))
                ->setName($controller->l('Load another template'))
                ->setOptions([
                    'templates' => $templates,
                    'link' => new Link(),
                    'ec_list' => $tab
                ])
        );
    }

    /**
     * @return $this
     */
    public function setDefaultJoin($list)
    {
        switch ($list) {
            case 'orders':
                if (version_compare(_PS_VERSION_, '1.7.7.0', '<') &&
                    version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
                    $this->select('`' . $this->getAlias('orders') . '`.`id_order`');
                }
                $this->already_join[] = "orders";
                $this->already_join[] = "customer";
                $this->already_join[] = "currency";
                $this->already_join[] = "address";
                $this->already_join[] = "order_state";
                $this->already_join[] = "shop";
                $this->already_join[] = "country";
                $this->already_join[] = "country_lang";

                $this->already_join[] = 'cart_rule';
                break;
            case 'customer':
                if (version_compare(_PS_VERSION_, '1.7.6.0', '<')) {
                    $this->select('`' . $this->getAlias('customer') . '`.`id_customer`');
                }
                $this->joinQuery(
                    'left',
                    $this->getAlias('customer'),
                    'customer_group',
                    'customer_group',
                    '`' . $this->getAlias('customer') . '`.`id_customer` = `customer_group`.`id_customer`'
                );
                $this->already_join[] = 'customer_group';
                break;
            case 'address':
                if (version_compare(_PS_VERSION_, '1.7.7.0', '<')) {
                    $this->select('`' . $this->getAlias('address') . '`.`id_address`');
                }
                break;
            case 'customer_thread':
                $this->select('`a`.`id_customer_thread`');
                break;
            case 'cart':
                $this->select('`a`.`id_cart`');

                $this->joinQuery(
                    'left',
                    'a',
                    'cart_product',
                    'cart_product',
                    '`a`.`id_cart` = `cart_product`.`id_cart`'
                )->joinQuery(
                    'left',
                    'cart_product',
                    'product',
                    'product',
                    '`cart_product`.`id_product` = `product`.`id_product`'
                );
                break;
        }

        return $this;
    }

    public function addDefaultJoin($list, $table)
    {
        switch ($list) {
            case 'orders':
                switch ($table) {
                    case 'shop':
                        if (!in_array('shop', $this->already_join)) {
                            $this->joinQuery(
                                'left',
                                $this->getAlias('orders'),
                                'shop',
                                $this->getAlias('shop'),
                                '`' . $this->getAlias('orders') . '`.`id_shop` = 
                                `' . $this->getAlias('shop') . '`.`id_shop`'
                            );

                            $this->already_join[] = 'shop';
                        }
                        break;
                    case 'currency':
                        if (!in_array('currency', $this->already_join)) {
                            $this->joinQuery(
                                'left',
                                $this->getAlias('orders'),
                                'currency',
                                $this->getAlias('currency'),
                                '`' . $this->getAlias('orders') . '`.`id_currency` = 
                                `' . $this->getAlias('currency') . '`.`id_currency`'
                            );

                            $this->already_join[] = 'currency';
                        }
                        break;
                    case 'order_detail':
                        if (!in_array('order_detail', $this->already_join)) {
                            $this->joinQuery(
                                'left',
                                $this->getAlias('orders'),
                                'order_detail',
                                'order_detail',
                                '`' . $this->getAlias('orders') . '`.`id_order` = `order_detail`.`id_order`'
                            );

                            $this->already_join[] = 'order_detail';
                        }
                        break;
                    case 'product':
                        $this->addDefaultJoin($list, 'order_detail');
                        if (!in_array('product', $this->already_join)) {
                            $this->joinQuery(
                                'left',
                                'order_detail',
                                'product',
                                'product',
                                '`order_detail`.`product_id` = `product`.`id_product`'
                            );

                            $this->already_join[] = 'product';
                        }
                        break;
                    case 'product_lang':
                        $this->addDefaultJoin($list, 'product');
                        if (!in_array('product_lang', $this->already_join)) {
                            $this->joinQuery(
                                'left',
                                'product',
                                'product_lang',
                                'product_lang',
                                '`product_lang`.`id_product` = `product`.`id_product` 
                                    AND `product_lang`.`id_lang` = ' . (int)$this->context->language->id
                            );

                            $this->already_join[] = 'product_lang';
                        }
                        break;
                    case 'carrier':
                        $this->addDefaultJoin($list, "order_carrier");
                        if (!in_array('carrier', $this->already_join)) {
                            $this->joinQuery(
                                'left',
                                $this->getAlias('order_carrier'),
                                'carrier',
                                'carrier',
                                '`order_carrier`.`id_carrier` = `carrier`.`id_carrier`'
                            );

                            $this->already_join[] = 'carrier';
                        }
                        break;
                    case 'order_carrier':
                        if (!in_array('order_carrier', $this->already_join)) {
                            $this->joinQuery(
                                'left',
                                $this->getAlias('orders'),
                                'order_carrier',
                                'order_carrier',
                                '`' . $this->getAlias('orders') . '`.`id_order` = `order_carrier`.`id_order`'
                            );

                            $this->already_join[] = 'order_carrier';
                        }
                        break;
                    case 'customer_group':
                        $this->addDefaultJoin($list, 'customer');
                        if (!in_array('customer_group', $this->already_join)) {
                            $this->joinQuery(
                                'left',
                                $this->getAlias('customer'),
                                'customer_group',
                                'customer_group',
                                '`' . $this->getAlias('customer') . '`.`id_customer` = `customer_group`.`id_customer`'
                            );

                            $this->already_join[] = 'customer_group';
                        }
                        break;
                    case 'customer':
                        if (!in_array('customer', $this->already_join)) {
                            $this->joinQuery(
                                'left',
                                $this->getAlias('orders'),
                                'customer',
                                $this->getAlias('customer'),
                                '`' . $this->getAlias('orders') . '`.`id_customer` = 
                                `' . $this->getAlias('customer') . '`.`id_customer`'
                            );

                            $this->already_join[] = 'customer';
                        }
                        break;
                    case 'address':
                        if (!in_array('address', $this->already_join)) {
                            $this->joinQuery(
                                'left',
                                $this->getAlias('orders'),
                                'address',
                                $this->getAlias('address'),
                                '`' . $this->getAlias('orders') . '`.`id_address_delivery` = 
                                `' . $this->getAlias('address') . '`.`id_address`'
                            );

                            $this->already_join[] = 'address';
                        }
                        break;
                    case 'order_state':
                        if (!in_array('order_state', $this->already_join)) {
                            $this->joinQuery(
                                'left',
                                $this->getAlias('orders'),
                                'order_state',
                                $this->getAlias('order_state'),
                                '`' . $this->getAlias('orders') . '`.`id_order_state` = 
                                `' . $this->getAlias('order_state') . '`.`id_order_state`'
                            );

                            $this->already_join[] = 'order_state';
                        }
                        break;
                    case 'country':
                        $this->addDefaultJoin($list, 'address');
                        if (!in_array('country', $this->already_join)) {
                            $this->joinQuery(
                                'inner',
                                $this->getAlias('address'),
                                'country',
                                $this->getAlias('country'),
                                '`' . $this->getAlias('address') . '`.`id_country` = 
                                `' . $this->getAlias('country') . '`.`id_country`'
                            );

                            $this->already_join[] = 'country';
                        }
                        break;
                    case 'country_lang':
                        $this->addDefaultJoin($list, 'country');
                        if (!in_array('country_lang', $this->already_join)) {
                            $this->joinQuery(
                                'inner',
                                $this->getAlias('country'),
                                'country_lang',
                                $this->getAlias('country_lang'),
                                '`' . $this->getAlias('country') . '`.`id_country` = 
                                `' . $this->getAlias('country_lang') . '`.`id_country` 
                                AND `' . $this->getAlias('country_lang') . '`.`id_lang` = 
                                ' . (int)$this->context->language->id
                            );

                            $this->already_join[] = 'country_lang';
                        }
                        break;
                    case 'image':
                        $this->addDefaultJoin($list, 'order_detail');
                        if (!in_array('image', $this->already_join)) {
                            $this->joinQuery(
                                'left',
                                $this->getAlias('order_detail'),
                                'image_shop',
                                'image',
                                "`order_detail`.`product_id` = `image`.`id_product` AND `image`.`id_shop` IN (" . implode(', ', Shop::getContextListShopID()) . ") AND `image`.`cover` = 1"
                            );

                            $this->already_join[] = 'image';
                            $this->addDefaultJoin($list, 'product_attribute_image');
                        }
                        break;
                    case 'product_attribute_image':
                        if (!in_array('product_attribute_image', $this->already_join)) {
                            $this->joinQuery(
                                'left',
                                $this->getAlias('order_detail'),
                                'product_attribute_image',
                                'product_attribute_image',
                                "`" . $this->getAlias('order_detail') . "`.`product_attribute_id` =
                                `product_attribute_image`.`id_product_attribute`"
                            );

                            $this->already_join[] = 'product_attribute_image';
                        }
                        break;
                }
                break;
        }
    }

    private function getConfigurations($tab)
    {
        $return = array();

        $id_profile = Db::getInstance()->getValue(
            "SELECT `id_extracolumns_profiles`
            FROM `" . _DB_PREFIX_ . $this->configuration_table . "_employee_profile` `ep`
            LEFT JOIN `" . _DB_PREFIX_ . $this->configuration_table . "` `a`
                ON `ep`.`id_extracolumns` = `a`.`id_extracolumns`
            WHERE `tab` = '" . pSQL($tab) . "' AND `id_employee` = " . (int)$this->context->employee->id
        );

        $id_profile = $id_profile === false ? 1 : $id_profile;

        $return['configurations'] = Db::getInstance()->getRow(
            "SELECT
                GROUP_CONCAT(`table` ORDER BY `position` ASC SEPARATOR ';') as 'tables',
                GROUP_CONCAT(`attribute` ORDER BY `position` ASC SEPARATOR ';') as 'attributes',
                GROUP_CONCAT(`title` ORDER BY `position` ASC SEPARATOR ';') as 'titles',
                GROUP_CONCAT(`type` ORDER BY `position` ASC SEPARATOR ';') as 'types'
            FROM `" . _DB_PREFIX_ . $this->configuration_table . "` `a`
            RIGHT JOIN `" . _DB_PREFIX_ . $this->configuration_table . "_columns` `c`
                ON `a`.`id_extracolumns` = `c`.`id_extracolumns`
            WHERE `tab` = '" . pSQL($tab) . "' AND `c`.`id_extracolumns_profiles` = " . (int)$id_profile
        );

        $this->getParameters($tab);

        $return['array_tables'] = explode(';', $return['configurations']['tables'] ?: "");

        foreach ($this->tables_default as $table_to_min) {
            if (!array_key_exists($table_to_min, $this->aliases)) {
                continue;
            }
            $return['array_tables'] = preg_replace(
                '/^' . $table_to_min . '$/',
                $this->getAlias($table_to_min),
                $return['array_tables']
            );
        }

        // Initialisation
        $return['array_attributes'] = explode(';', $return['configurations']['attributes'] ?: "");
        $return['config_tables'] = explode(';', $return['configurations']['tables'] ?: "");
        $return['array_titles'] = explode(';', $return['configurations']['titles'] ?: "");
        $return['array_types'] = explode(';', $return['configurations']['types'] ?: "");

        return $return;
    }

    private function getParameters($tab)
    {
        switch ($tab) {
            case 'orders':
                $this->aliases = array(
                    'orders' => (version_compare(_PS_VERSION_, '1.7.7.0', '<') ? 'a' : 'o'),
                    'shop' => (version_compare(_PS_VERSION_, '1.7.7.0', '<') ? 'shop' : 's'),
                    'customer' => (version_compare(_PS_VERSION_, '1.7.7.0', '<') ? 'c' : 'cu'),
                    'address' => (version_compare(_PS_VERSION_, '1.7.7.0', '<') ? 'address' : 'a'),
                    'country' => (version_compare(_PS_VERSION_, '1.7.7.0', '<') ? 'country' : 'c'),
                    'country_lang' => (version_compare(_PS_VERSION_, '1.7.7.0', '<') ? 'country_lang' : 'cl'),
                    'order_state' => 'os',
                    'order_state_lang' => 'osl',
                    'order_detail' => 'order_detail',
                    'order_carrier' => 'order_carrier',
                    'customer_group' => 'customer_group',
                    'currency' => 'cur',
                );

                $this->fields = array(
                    $this->getAlias('orders') => 'id_order',
                    'a2' => array(
                        'base_on' => 'reference',
                        'join_on' => 'order_reference',
                        'table' => $this->getAlias('orders')
                    ),
                    $this->getAlias('customer') => 'id_customer',
                    $this->getAlias('address') => 'id_address',
                    $this->getAlias('country') => 'id_country',
                    $this->getAlias('order_state') => 'id_order_state',
                    $this->getAlias('order_detail') => 'id_customization',
                    $this->getAlias('order_carrier') => 'id_carrier',
                    $this->getAlias('customer_group') => 'id_group',
                    'product' => 'id_product',
                    'p2' => array(
                        'base_on' => 'id_supplier',
                        'join_on' => 'id_supplier',
                        'table' => 'product'
                    )
                );

                $this->tables_default = array(
                    'orders',
                    'shop',
                    'customer',
                    // 'currency',
                    'address',
                    'country',
                    'country_lang',
                    'order_state',
                    'order_state_lang',
                    'order_detail',
                    'product',
//                    'product_lang',
                    'order_carrier',
//                    'carrier',
                    'customer_group',
                    'image',
                );

                // No changes is needed for the aliases
                // TODO route parameter
                $this->select_fields = array(
                    'order_state_langname' => array(
                        'id' => 'id_order_state',
                        'table' => 'order_state_lang',
                        'name' => 'name',
                        'lang' => true
                    ),
                    'country_langname' => array(
                        'id' => 'id_country',
                        'table' => 'country_lang',
                        'name' => 'name',
                        'join' => " INNER JOIN `" . _DB_PREFIX_ . "country` c ON a.id_country = c.id_country
                            INNER JOIN `" . _DB_PREFIX_ . "address` ad ON c.id_country = ad.id_country
                            INNER JOIN `" . _DB_PREFIX_ . "orders` o ON `ad`.`id_address` = `o`.`id_address_delivery`",
                        'lang' => true
                    ),
                    'orderspayment' => array(
                        'id' => 'payment',
                        'table' => 'orders',
                        'name' => 'payment',
                        'lang' => false
                    ),
                );
                break;

            case 'customer':
                $this->tables_default = array(
                    'customer',
                    'gender_lang',
                    'shop'
                );

                $this->aliases = array(
                    'customer' => (version_compare(_PS_VERSION_, '1.7.6.0', '<') ? 'a' : 'c'),
                    'gender_lang' => 'gl',
                    'shop' => 's'
                );

                $this->fields = array(
                    $this->getAlias('customer') => 'id_customer',
                    'customer_group' => 'id_group'
                );

                $this->select_fields = array(
                    'gender_langname' => array(
                        'id' => 'id_gender',
                        'table' => 'gender_lang',
                        'name' => 'name',
                        // 'join' => " INNER JOIN `" . _DB_PREFIX_ . "country` c ON a.id_country = c.id_country
                        //     INNER JOIN `" . _DB_PREFIX_ . "address` ad ON c.id_country = ad.id_country",
                        'lang' => true
                    ),
                );
                break;

            case 'address':
                $this->aliases = array(
                    'address' => 'a',
                    'country_lang' => 'cl',
                    'customer' => (version_compare(_PS_VERSION_, '1.7.7.0', '<') ? 'c' : 'customer')
                );

                $this->tables_default = array(
                    'address',
                    'country_lang',
                    'customer'
                );

                if (version_compare(_PS_VERSION_, '1.7.7.0', '<')) {
                    $this->select_fields = array(
                        'country_langname' => array(
                            'id' => 'id_country',
                            'table' => 'country_lang',
                            'name' => 'name',
                            'join' => " INNER JOIN `" . _DB_PREFIX_ . "country` c ON a.id_country = c.id_country
                                INNER JOIN `" . _DB_PREFIX_ . "address` ad ON c.id_country = ad.id_country",
                            'lang' => true
                        ),
                    );
                } else {
                    $this->tables_default[] = 'country';
                    $this->aliases['country'] = 'c';
                }

                $this->fields = array(
                    $this->getAlias('address') => 'id_address',
                    'a1' => array(
                        'base_on' => 'id_address',
                        'join_on' => 'id_address_delivery',
                        'table' => $this->getAlias('address')
                    ),
                    'a2' => array(
                        'base_on' => 'id_address',
                        'join_on' => 'id_address_invoice',
                        'table' => $this->getAlias('address')
                    ),
                    $this->getAlias('customer') => 'id_customer',
                    $this->getAlias('country_lang') => 'id_country',
                );
                break;

            case 'cart':
                $this->fields = array(
                    'a' => 'id_cart',
                    'c' => 'id_customer',
                    'cu' => 'id_currency',
                    'ca' => 'id_carrier',
                    'co' => 'id_guest',
                    'cart_product' => 'id_customization'
                );

                $this->tables_default = array(
                    'cart',
                    'customer',
                    'carrier',
                    'orders',
                    'connections',
                    'cart_product',
                    'product'
                );

                $this->aliases = array(
                    'cart' => 'a',
                    'customer' => 'c',
                    'carrier' => 'ca',
                    'orders' => 'o',
                    'connections' => 'co'
                );
                break;

            case 'customer_thread':
                $this->fields = array(
                    'a' => 'id_customer_thread',
                    'c' => 'id_customer',
                    'cl' => 'id_contact',
                    'co' => 'id_guest',
                    'l' => 'id_lang',
                );

                $this->tables_default = array(
                    'customer_thread',
                    'customer',
                    'customer_message',
                    'contact_lang',
                    'lang',
                );

                $this->aliases = array(
                    'customer_thread' => 'a',
                    'customer' => 'c',
                    'customer_message' => 'cm',
                    'contact_lang' => 'cl',
                    'lang' => 'l'
                );
                break;
        }

        $this->setParameters($tab);
    }

    public function getColumns($tab, $with_types = false)
    {
        if ($with_types) {
            $types = array();
            foreach ($this->getTypesToModify() as $type) {
                $types[] = "`b`.`type` = '" . $type . "'";
            }
        }

        $id_profile = Db::getInstance()->getValue(
            "SELECT `id_extracolumns_profiles`
            FROM `" . _DB_PREFIX_ . $this->configuration_table . "_employee_profile` `ep`
            LEFT JOIN `" . _DB_PREFIX_ . $this->configuration_table . "` `a`
                ON `ep`.`id_extracolumns` = `a`.`id_extracolumns`
            WHERE `tab` = '" . pSQL($tab) . "' AND `id_employee` = " . (int)$this->context->employee->id
        );

        $id_profile = $id_profile === false ? 1 : $id_profile;

        return Db::getInstance()->executeS(
            "SELECT `b`.`table`, `b`.`attribute`, `b`.`title`, `b`.`type`
            FROM `" . _DB_PREFIX_ . $this->configuration_table . "` `a`
            RIGHT JOIN `" . _DB_PREFIX_ . $this->configuration_table . "_columns` `b`
                ON `a`.`id_extracolumns` = `b`.`id_extracolumns`
            WHERE `a`.`tab` = '" . pSQL($tab) . "'
                AND (" . ($with_types ? implode(' OR ', $types) : "1") . ")
                AND `b`.`id_extracolumns_profiles` = " . (int)$id_profile . "
            ORDER BY `b`.`position`"
        );
    }

    public function getAttributeAs($table, $attribute)
    {
        $attribute_as = pSQL($table) . pSQL($attribute);

        if (array_key_exists($attribute_as, $this->attributes_as_list)) {
            $attribute_as = $this->attributes_as_list[$attribute_as];
        }

        return $attribute_as;
    }

    /**
     * @param \PrestaShop\PrestaShop\Core\Grid\Definition\GridDefinition $definition
     */
    public function addExportGridAction($definition)
    {
        $actions = $definition->getGridActions();

        $actions->add(
            (new PrestaShop\PrestaShop\Core\Grid\Action\Type\LinkGridAction('export'))
                ->setName($this->context->getTranslator()->trans('Export'))
                ->setIcon('cloud_download')
                ->setOptions([
                    'route' => 'admin_extra_columns_export_' . $definition->getId(),
                ])
        );
    }

    public function isActive()
    {
        return true;
    }

    public function getTypesToModify()
    {
        return array(
            'price',
            'percent'
        );
    }

    /**
     * @param mixed $value
     * @param Context $context
     * @return mixed
     */
    public function modifyToPrice($value, $tr, $locale)
    {
        if (Validate::isFloat($value)) {
            return $locale->formatPrice($value, $tr['iso_code']);
        } else {
            return $value;
        }
    }

    public function modifyToPercent($value)
    {
        if (Validate::isPercentage($value)) {
            return ((float)round($value, 2)) . " %";
        } else {
            return $value;
        }
    }

    public function printTrackingNumberUrl($value, $row)
    {
        $return = "<span class='ec_tracking_url' data-url='" . ($row['carrier_url'] ? pSQL($row['carrier_url']) : '') . "'>";

        if ($row['carrier_url'] && $value && !preg_match('/;/', $value)) {
            $url = preg_replace('/@/', $value, $row['carrier_url']);
            $return .= '<a target="_blank" href="' . pSQL($url) . '">' . $value . '</a>';
        } else {
            $return .= $value;
        }
        $return .= '</span>';
        return $return;
    }

    public function printSumInteger($echo)
    {
        return $this->printSum($echo, 0);
    }

    public function printSumDecimal($echo)
    {
        return $this->printSum($echo, 2);
    }

    public function printSum($echo, $decimals = 0)
    {
        $explode = explode(';', $echo);

        $sum = 0;
        foreach ($explode as $row) {
            $values = explode(':', $row);

            $sum += isset($values[1]) ? (float)$values[1] : 0;
        }

        return number_format($sum, $decimals);
    }

    public function printBoolean($echo)
    {
        return $echo ?
            Module::getInstanceByName('extracolumns')->l('Yes') :
            Module::getInstanceByName('extracolumns')->l('No');
    }

    public function getAlias($table)
    {
        if (array_key_exists($table, $this->aliases)) {
            return $this->aliases[$table];
        } else {
            return $table;
        }
    }

    public function printProductsThumbnails($echo, $tr)
    {
        $this->context->smarty->assign([
            'ec_products_infos' => $this->getProductsImageInfos($echo, $tr),
            'ec_order_id' => $tr['id_order'],
            'image_size' => 40,
        ]);

        return $this->context->smarty->fetch(_PS_MODULE_DIR_ . 'extracolumns/views/templates/admin/printProductsImage.tpl');
    }

    public function getProductsImageInfos($echo, $tr)
    {
        $images = explode(';', $echo);
        $ids_product = explode(';', $tr['ec_image_id_product']);
        $ids_product_attribute = explode(';', $tr['ec_image_id_product_attribute']);

        $products = [];
        foreach ($images as $key => $image) {
            $explode = explode(':', $image);
            $quantity = (int)$explode[0];
            $id_image = (int)$explode[1];
            $id_product = array_key_exists($key, $ids_product) ? (int)explode(':', $ids_product[$key])[1] : 0;
            $id_product_attribute = array_key_exists($key, $ids_product_attribute) ? (int)explode(':', $ids_product_attribute[$key])[1] : 0;

            $products[] = [
                'image' => Tools::getShopDomainSsl(true)._PS_IMG_.
                    'p/'.Image::getImgFolderStatic($id_image).(int)$id_image.'.jpg',
                'name' => $quantity . 'x ' . Product::getProductName(
                        $id_product,
                        $id_product_attribute
                )
            ];
        }

        return $products;
    }
}
