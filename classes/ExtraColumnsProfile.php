<?php
/**
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class ExtraColumnsProfile extends ObjectModel
{
    /** @var string Name */
    public $id_extracolumns_profiles;
    public $id_extracolumns;
    public $name;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'extracolumns_profiles',
        'primary' => 'id_extracolumns_profiles',
        'multilang' => false,
        'fields' => array(
            'id_extracolumns' => array(
                'type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true
            ),
            'name' => array(
                'type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => true
            ),
        ),
    );

    public function add($auto_date = true, $null_values = false)
    {
        if (!$this->nameIsUnique()) {
            throw new PrestaShopModuleException($this->trans('This name is already used by another template.'));
        }
        return parent::add($auto_date, $null_values);
    }

    public function update($null_values = false)
    {
        if (!$this->nameIsUnique()) {
            throw new PrestaShopModuleException($this->l('This name is already used by another template'));
        }
        parent::update($null_values);
    }

    public static function getProfiles($id_extracolums)
    {
        return Db::getInstance()->executeS(
            "SELECT *
            FROM `" . _DB_PREFIX_ . "extracolumns_profiles`
            WHERE `id_extracolumns` = " . (int)$id_extracolums
        );
    }

    public function delete()
    {
        if (!parent::delete()) {
            return false;
        }

        $return = Db::getInstance()->delete(
            'extracolumns_employee_profile',
            'id_extracolumns_profiles = ' . (int)$this->id
        );

        $return &= Db::getInstance()->delete(
            'extracolumns_columns',
            'id_extracolumns_profiles = ' . (int)$this->id
        );

        return $return;
    }

    public static function getProfilesByTabName($tab)
    {
        return Db::getInstance()->executeS(
            "SELECT `p`.*
            FROM `" . _DB_PREFIX_ . "extracolumns_profiles` `p`
            LEFT JOIN `" . _DB_PREFIX_ . "extracolumns` `b`
                ON `p`.`id_extracolumns` = `b`.`id_extracolumns`
            WHERE `b`.`tab` = '" . pSQL($tab) . "'"
        );
    }

    public static function getProfileByEmployee($id_employee, $tab)
    {
        return (int)Db::getInstance()->getValue(
            "SELECT `a`.`id_extracolumns_profiles`
            FROM `" . _DB_PREFIX_ . "extracolumns_employee_profile` `a`
            LEFT JOIN `" . _DB_PREFIX_ . "extracolumns` `b`
                ON `a`.`id_extracolumns` = `b`.`id_extracolumns`
            WHERE `b`.`tab` = '" . pSQL($tab) . "'
                AND `a`.`id_employee` = " . (int)$id_employee
        );
    }

    public static function updateProfileForEmployee($tab, $id_employee, $id_profile)
    {
        $data = array(
            'id_extracolumns_profiles' => (int)$id_profile,
            'id_employee' => (int)$id_employee,
            'id_extracolumns' => (int)Db::getInstance()->getValue(
                "SELECT `id_extracolumns`
                FROM `" . _DB_PREFIX_ . "extracolumns`
                WHERE `tab` = '" . pSQL($tab) . "'"
            )
        );

        return Db::getInstance()->insert(
            'extracolumns_employee_profile',
            $data,
            false,
            true,
            Db::REPLACE
        );
    }

    private function nameIsUnique()
    {
        $where = (!empty($this->id) ? " AND `id_extracolumns_profiles` <> " . (int)$this->id : "");
        $is_unique = Db::getInstance()->getValue(
            "SELECT COUNT(*)
            FROM `" . _DB_PREFIX_ . "extracolumns_profiles`
            WHERE `id_extracolumns` = " . (int)$this->id_extracolumns . "
            AND `name` = '" . pSQL($this->name) . "'" . $where
        );

        return !(bool)$is_unique;
    }
}
