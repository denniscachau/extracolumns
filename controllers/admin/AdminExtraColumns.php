<?php
/**
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminExtraColumnsController extends ModuleAdminController
{
    protected $position_identifier = 'id_extracolumns_columns';
    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'extracolumns_columns';
        $this->identifier = 'id_extracolumns_columns';
        $this->className = 'ExtraColumnsConfiguration';
        $this->tabClassName = 'AdminExtraColumns';
        $this->lang = false;
        $this->deleted = false;
        $this->colorOnBackground = false;
        $this->bulk_actions = array(
          'delete' => array('text' => 'Delete selected', 'confirm' => 'Delete selected items?'),
        );
        $this->context = Context::getContext();

        // $this->_select = "GROUP_CONCAT(`attribute` SEPARATOR '; ') AS 'list_attributes'";
        $this->_where = "AND `id_extracolumns` = '" . (int)$this->context->cookie->ec_list . "'";
        $this->_where .= " AND `id_extracolumns_profiles` = 1";
        $this->_defaultOrderBy = 'position';

        parent::__construct();

        $this->fields_list = array(
            'position' => array(
                'title' => $this->l('Position'),
                'align' => 'text-center',
                'position' => 'position',
                'filter_key' => 'position',
                'class' => 'fixed-width-xs',
                'search' => false,
                'orderby' => false
            ),
            'attribute' => array(
                'title' => $this->l('Attributes'),
                'align' => 'text-center',
                'filter_key' => 'attribute',
                'search' => false,
                'orderby' => false
            ),
            'table' => array(
                'title' => $this->l('Table'),
                'align' => 'text-center',
                'filter_key' => 'table',
                'search' => false,
                'orderby' => false
            ),
            'title' => array(
                'title' => $this->l('Title'),
                'align' => 'text-center',
                'class' => 'ec-titles',
                'search' => false,
                'orderby' => false
            ),
            'type' => array(
                'title' => $this->l('Type'),
                'align' => 'text-center',
                'class' => 'ec-types',
                'search' => false,
                'orderby' => false
            ),
        );

        $this->orders_column = array(
            'id_order',
            'id_customer',
            'id_address',
            'id_country',
            'id_order_state',
            'id_carrier',
            'id_group',
            'id_product',
            'id_supplier',
            'order_reference'
        );

        $this->orders_table = array(
            _DB_PREFIX_ . 'image',
        );

        if (version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
            $this->orders_table[] = _DB_PREFIX_ . 'customized_data';
        }

        $this->customer_column = array(
            'id_customer',
            'id_group'
        );

        $this->customer_table = array();

        $this->address_column = array(
            'id_customer',
            'id_address',
            'id_country',
        );

        $this->address_table = array();

        $this->cart_column = array(
            'id_cart',
            'id_customer',
            'id_currency',
            'id_carrier',
            'id_guest',
        );

        $this->cart_table = array(
            _DB_PREFIX_ . 'product'
        );

        if (version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
            $this->cart_table[] = _DB_PREFIX_ . 'customized_data';
        }

        $this->customer_thread_column = array(
            'id_customer_thread',
            'id_customer',
            'id_contact',
            'id_guest',
        );

        $this->customer_thread_table = array();

        $this->types = array(
            'boolean' => $this->l('Boolean (True / False)'),
            'price' => $this->l('Price'),
            'percent' => $this->l('Percentage'),
            'date' => $this->l('Date'),
            'datetime' => $this->l('Date / time'),
            'select' => $this->l('Select filter (only availbale for some fields)')
        );

        $this->quick_add = array(
            'orders' => array(
                $this->l('Default') => array(
                    'orders-reference',
                    'new' => 'extracolumns-new_client',
                    'country_lang-name',
                    'extracolumns-customer',
                    'orders-total_paid_tax_incl',
                    'orders-payment',
                    version_compare(_PS_VERSION_, '1.7.7.0', '<') ? 'order_state_lang-name' : 'orders-current_state',
                    'orders-date_add'
                ),
                $this->l('Order\'s information') => array(
                    'order_carrier-tracking_number',
                    'carrier-name',
                    'orders-total_paid_tax_excl',
                    'orders-invoice_number',
                    // 'extracolumns-invoice_name',
                    'order_cart_rule-name',
                    // 'order_cart_rule-value',
                    'extracolumns-sum_discount',
                    'cart_rule-code',
                    'order_payment-payment_method'
                ),
                $this->l('Customer\'s information') => array(
                    'customer-firstname',
                    'customer-lastname',
                    'customer-email',
                    'address-phone',
                    'address-address1',
                    'billing_address-address1',
                    'address-address2',
                    'billing_address-address2',
                    'address-company',
                    'billing_address-company',
                    'customer-note',
                    'extracolumns-default_group',
                    'extracolumns-total_spent'
                ),
                $this->l('Products information') => array(
                    'image-id_image',
                    'product_lang-name',
                    'product-reference',
                    // 'supplier-name',
                    'product-ean13',
                    'product-isbn',
                    'product-upc',
                    'order_detail-product_weight',
                    // 'customized_data' => 'customized_data-value'
                ),
            ),
            'customer' => array(
                $this->l('Default') => array(
                    'gender_lang-name',
                    'customer-firstname',
                    'customer-lastname',
                    'customer-email',
                    'extracolumns-total_spent',
                    'customer-active',
                    'customer-newsletter',
                    'customer-optin',
                    'customer-date_add',
                    'extracolumns-last_visit'
                ),
                $this->l('B2B') => array(
                    'customer-company',
                    'customer-siret',
                    'customer-ape',
                ),
                $this->l('Others') => array(
                    'customer-note',
                    'customer-birthday',
                    'extracolumns-nb_orders',
                    'extracolumns-default_group',
                    'group_lang-name'
                ),
            ),
            'address' => array(
                $this->l('Default') => array(
                    'customer-firstname',
                    'customer-lastname',
                    'address-address1',
                    'address-postcode',
                    'address-city',
                    'country_lang-name'
                ),
                $this->l('Others') => array(
                    'address-address2',
                    'address-firstname',
                    'address-lastname',
                    'address-phone',
                    'address-company',
                    'address-alias',
                    'orders-reference'
                ),
            ),
            'customer_thread' => array(
                $this->l('Default') => array(
                    'customer-firstname',
                    'customer-lastname',
                    'customer_thread-email',
                    'contact_lang-name',
                    'lang-name',
                    'customer_thread-status',
                    'customer_message-message',
                    'customer_thread-date_upd'
                ),
                $this->l('Others') => array(
                ),
            ),
            'cart' => array(
                $this->l('Default') => array(
                    'cart-status',
                    'customer-firstname',
                    'customer-lastname',
//                    'cart-total',
                    'carrier-name',
                    'cart-date_upd'
                ),
                $this->l('Others') => array(
                    'product-reference',
                    'customized_data' => 'customized_data-value'
                ),
            ),
        );
        if (version_compare(_PS_VERSION_, '1.7.0.0', '<')) {
            unset($this->quick_add['orders'][$this->l('Products information')]['customized_data']);
            unset($this->quick_add['cart'][$this->l('Others')]['customized_data']);
        }
    }

    public function initPageHeaderToolbar()
    {
        parent::initPageHeaderToolbar();

        if (empty($this->display)) {
            $this->page_header_toolbar_btn['desc-module-back'] = array(
                'href' => 'index.php?controller=AdminModules&configure=extracolumns&token=' .
                Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Other tabs'),
                'icon' => 'process-icon-back',
            );
            $this->page_header_toolbar_btn['desc-module-new'] = array(
                'href' => 'index.php?controller=' . $this->tabClassName . '&add' . $this->table .
                    '&token=' . Tools::getAdminTokenLite($this->tabClassName),
                'desc' => $this->l('Add a new column'),
                'icon' => 'process-icon-new',
            );
            $this->page_header_toolbar_btn['desc-module-doc'] = array(
                'href' => _MODULE_DIR_ . 'extracolumns/readme_' . $this->getDocSuffix() . '.pdf',
                'desc' => $this->l('Read the documentation'),
                'target' => true,
                'icon' => 'process-icon- icon-file-text',
            );
        }
    }

    private function getDocSuffix()
    {
        $available_docs = array(
            'fr'
        );

        return in_array($this->context->language->iso_code, $available_docs) ?
            $this->context->language->iso_code :
            'en';
    }

    public function initProcess()
    {
        if (Tools::isSubmit('submitAddextracolumns_columns') || Tools::isSubmit('submitQuickAdd')) {
            $lastPosition = ExtraColumnsConfiguration::getLastPosition(Tools::getValue('id_extracolumns'));
            $i = 1;
            $positions = array();
            $attributes = array();
            $tables = array();
            $titles = array();
            $types = array();
        }

        if (Tools::isSubmit('submitAddextracolumns_columns')) {
            while (Tools::getValue('EXTRACOLUMNS_ATTRIBUTE_' . $i)) {
                if (version_compare(_PS_VERSION_, '1.6.1.0', '<')) {
                    $positions[] = $lastPosition + $i;
                } else {
                    $positions[] = $lastPosition + $i - 1;
                }
                $attributes[] = Tools::getValue('EXTRACOLUMNS_ATTRIBUTE_' . $i);
                $tables[] = Tools::getValue('EXTRACOLUMNS_TABLE_' . $i);
                $titles[] = Tools::getValue('EXTRACOLUMNS_TITLE_' . $i);
                $types[] = Tools::getValue('EXTRACOLUMNS_TYPE_' . $i);
                ++$i;
            }
        } elseif (Tools::isSubmit('submitQuickAdd')) {
            if (version_compare(_PS_VERSION_, '1.6.1.0', '<')) {
                $positions[] = $lastPosition + 1;
            } else {
                $positions[] = $lastPosition;
            }

            $attribute_to_add = explode('-', Tools::getValue('EXTRACOLUMNS_QUICK_ADD'), 2);
            $default_list = $this->getDefaultList()[Tools::getValue('EXTRACOLUMNS_QUICK_ADD')];

            $tables[] = $attribute_to_add[0];
            $attributes[] = $attribute_to_add[1];
            $titles[] = isset($default_list['title']) ? $default_list['title'] : "";
            $types[] = $default_list['type'];
        }

        if (Tools::isSubmit('submitAddextracolumns_columns') || Tools::isSubmit('submitQuickAdd')) {
            $_POST['positions'] = implode(';', $positions);
            $_POST['attributes'] = implode(';', $attributes);
            $_POST['tables'] = implode(';', $tables);
            $_POST['titles'] = implode(';', $titles);
            $_POST['types'] = implode(';', $types);
        }

        parent::initProcess();

        if (!Tools::getIsset('ec_list')
            && (!$this->context->cookie->ec_list
            || !ExtraColumnsConfiguration::listExists($this->context->cookie->ec_list))
        ) {
            if (version_compare(_PS_VERSION_, '1.7.6.0', '<')) {
                $link = $this->context->link->getAdminLink('AdminModules', true) .
                    '&configure=extracolumns';
            } else {
                $link = $this->context->link->getAdminLink(
                    'AdminModules',
                    true,
                    [],
                    ['configure' => 'extracolumns']
                );
            }
            Tools::redirectAdmin($link);
        }

        $id_extracolumns = (int)$this->context->cookie->ec_list;

        if (Tools::isSubmit('submitGlobalForm')) {
            $obj = new ExtraColumnsConfiguration();
            if ($obj->populateExtracolumns(
                $id_extracolumns,
                Tools::getValue('replace'),
                Tools::getValue('start')
            )) {
                $link = new Link();
                if (version_compare(_PS_VERSION_, '1.7.6.0', '<')) {
                    $link = $link->getAdminLink('AdminExtraColumns', true) .
                        '&conf=4';
                } else {
                    $link = $link->getAdminLink(
                        'AdminExtraColumns',
                        true,
                        [],
                        ['conf' => 4]
                    );
                }
                Tools::redirectAdmin($link);
            } else {
                $this->errors[] = $this->l("An error has occurred.");
            }
        } elseif (Tools::isSubmit('submitQuickAdd')) {
            $obj = new ExtraColumnsConfiguration();
            try {
                $obj->add();
            } catch (Exception $e) {
                $this->errors[] = $e->getMessage();
            }

            if (version_compare(_PS_VERSION_, '1.7.6.0', '<')) {
                $link = $this->context->link->getAdminLink('AdminExtraColumns', true) . '&conf=4';
            } else {
                $link = $this->context->link->getAdminLink(
                    'AdminExtraColumns',
                    true,
                    [],
                    ['conf' => 4]
                );
            }
            Tools::redirectAdmin($link);
        }
    }

    public function initToolbar()
    {
        $this->toolbar_btn['save_template'] = array(
            'href' => '#',
            'imgclass' => 'save',
            'desc' => $this->l('Save as template')
        );
        if (!empty(ExtraColumnsProfile::getProfiles($this->context->cookie->ec_list))) {
            $this->toolbar_btn['manage_template'] = array(
                'href' => '#',
                'imgclass' => 'edit',
                'desc' => $this->l('Manage templates')
            );
            $this->toolbar_btn['load_template'] = array(
                'href' => '#',
                'imgclass' => 'save-date',
                'desc' => $this->l('Load template')
            );
        }

        parent::initToolbar();
    }

    public function renderList()
    {
        $id_extracolumns = (int)$this->context->cookie->ec_list;

        $this->addRowAction('delete');

        $config = Db::getInstance()->getRow(
            "SELECT `tab`, `replace`, `start`
            FROM `" . _DB_PREFIX_ . "extracolumns`
            WHERE `id_extracolumns` = '" . (int)$id_extracolumns . "'"
        );

        // $this->l('orders');
        // $this->l('customer');
        // $this->l('address');
        // $this->l('cart');
        // $this->l('customer_thread');

        $this->context->smarty->assign(array(
            'replace' => isset($config['replace']) ? $config['replace'] : 0,
            'start' => $config['start'],
            'types' => $this->types,
            'quick_add' => isset($config['tab']) ? $this->quick_add[$config['tab']] : array(array()),
            'description' => $this->getDefaultList($config['tab']),
            'id_extracolumns' => (int)$id_extracolumns,
            'profiles' => ExtraColumnsProfile::getProfiles($this->context->cookie->ec_list),
            'ec_link' => $this->context->link,
            'title' => $this->l('Customize columns') . ': ' . $this->l($config['tab'])
        ));

        $global_form = $this->context->smarty->fetch($this->module->getLocalPath() .
        'views/templates/admin/global_form.tpl');
        $config = $this->context->smarty->fetch($this->module->getLocalPath() .
            'views/templates/admin/config.tpl');
        $renderList = $this->context->smarty->fetch($this->module->getLocalPath() .
        'views/templates/admin/renderList.tpl');

        return $global_form . parent::renderList() . $config . $renderList;
    }

    public function ajaxProcessUpdatePositions()
    {
        $way = (int)Tools::getValue('way');
        $id_extracolumns_columns = (int)Tools::getValue('id');
        $positions = Tools::getValue('extracolumns_columns');

        $new_positions = array();
        foreach ($positions as $v) {
            if (count(explode('_', $v)) == 4) {
                $new_positions[] = $v;
            }
        }

        foreach ($new_positions as $position => $value) {
            $pos = explode('_', $value);

            if (isset($pos[2]) && (int)$pos[2] === $id_extracolumns_columns) {
                if ($extracolumns = new ExtraColumnsConfiguration()) {
                    Configuration::updateValue('id_extracolumns_columns', $id_extracolumns_columns);
                    if (isset($position) && $extracolumns->updatePosition(
                        $way,
                        $position
                    )) {
                        echo 'ok position '.(int)$position.' for id '.(int)$pos[1].'\r\n';
                    } else {
                        echo '{"hasError" : true, "errors" : "Can not update id ' . (int)$id_extracolumns_columns .
                        ' to position ' . (int)$position . ' "}';
                    }
                }

                break;
            }
        }
    }

    public function renderForm()
    {
        // Recover the tab that the user want to modify
        $tab = $this->context->cookie->ec_list ?
        Db::getInstance()->getValue(
            "SELECT `tab`
            FROM `" . _DB_PREFIX_ . "extracolumns`
            WHERE `id_extracolumns` = '" . (int)$this->context->cookie->ec_list . "'"
        ) : false;

        // For new tab
        if ($tab) {
            Configuration::updateValue('tab', $tab);

            $columns_name = $this->{$tab . '_column'};
            $table_name = $this->{$tab . '_table'};

            // Recover the modules tables that are compatible with the tab (with the corresponding id in attribute)
            // (ex. : id_order for the tab orders)
            $query = "SELECT `table_name` as 'table_name'
            FROM `information_schema`.`columns`
            WHERE `table_schema` = '" . _DB_NAME_ . "'
            AND `table_name` LIKE '" . str_replace('_', '\_', _DB_PREFIX_) . "%'
            AND (`column_name` = '" . implode("' OR `column_name` = '", $columns_name) . "')
            " . (!empty($table_name) ? "OR (`table_name` = '" . implode("' OR `table_name` = '", $table_name) . "')" :
            "") . " GROUP BY `table_name` ORDER BY `table_name`";

            $listModules = Db::getInstance()->executeS(
                $query
            );
        } else {
            $link = $this->context->link->getAdminLink('AdminExtraColumns', true);
            Tools::redirectAdmin($link);
        }

        $config = Db::getInstance()->getRow(
            "SELECT `replace`, `tab`
            FROM `" . _DB_PREFIX_ . "extracolumns`
            WHERE `id_extracolumns` = '" . (int)$this->context->cookie->ec_list . "'"
        );
        // Assign Positions
        $positions = array();
        if (Tools::getValue('EXTRACOLUMNS_GLOBAL_ATT')) {
            $positions['attributes'] = explode(';', Tools::getValue('EXTRACOLUMNS_GLOBAL_ATT'));
            $positions['tables'] = explode(';', Tools::getValue('EXTRACOLUMNS_GLOBAL_TAB'));
            $positions['titles'] = explode(';', Tools::getValue('EXTRACOLUMNS_GLOBAL_TIT'));
            $positions['types'] = explode(';', Tools::getValue('EXTRACOLUMNS_GLOBAL_TYP'));
        }
        $replace = $config['replace'];

        // Assign the default table (table_name)
        $selected_table = Tools::getValue('selecModule') ?
        Tools::getValue('selecModule') : $listModules[0]['table_name'];
        $attributes_available = array();

        if (!Tools::getValue('default') && Tools::getValue('selecModule')) {
            // Assign the available attributes
            $attributes_available[$selected_table] = Db::getInstance()->executeS(
                "SHOW columns FROM `" . pSQL($selected_table) . "`"
            ); // For new tab
        } else {
            if ($config['tab'] == 'orders') {
                $attributes_available[_DB_PREFIX_ . 'orders'] = Db::getInstance()->executeS(
                    'SHOW columns FROM `' . _DB_PREFIX_ . 'orders` 
                    WHERE `Field` NOT LIKE "id_%" AND `Field` <> "shipping_number"'
                );
                $attributes_available[_DB_PREFIX_ . 'customer'] = Db::getInstance()->executeS(
                    'SHOW columns FROM `' . _DB_PREFIX_ . 'customer` WHERE `Field` NOT LIKE "id_%"'
                );
                $attributes_available[_DB_PREFIX_ . 'address'] = Db::getInstance()->executeS(
                    'SHOW columns FROM `' . _DB_PREFIX_ . 'address` WHERE `Field` NOT LIKE "id_%"'
                );
                $attributes_available[_DB_PREFIX_ . 'country'] = Db::getInstance()->executeS(
                    'SHOW columns FROM `' . _DB_PREFIX_ . 'country` WHERE `Field` NOT LIKE "id_%"'
                );
                $attributes_available[_DB_PREFIX_ . 'country_lang'] = Db::getInstance()->executeS(
                    'SHOW columns FROM `' . _DB_PREFIX_ . 'country_lang` WHERE `Field` NOT LIKE "id_%"'
                );
                $attributes_available[_DB_PREFIX_ . 'order_state'] = Db::getInstance()->executeS(
                    'SHOW columns FROM `' . _DB_PREFIX_ . 'order_state` WHERE `Field` NOT LIKE "id_%"'
                );
                if (version_compare(_PS_VERSION_, '1.7.7.0', '<')) {
                    $attributes_available[_DB_PREFIX_ . 'order_state_lang'] = Db::getInstance()->executeS(
                        'SHOW columns FROM `' . _DB_PREFIX_ . 'order_state_lang` WHERE Field NOT LIKE "id_%"'
                    );
                }
                $attributes_available[_DB_PREFIX_ . 'product'] = Db::getInstance()->executeS(
                    'SHOW columns FROM `' . _DB_PREFIX_ . 'product` WHERE `Field` NOT LIKE "id_%"'
                );
            } elseif ($config['tab'] == 'customer') {
                $attributes_available[_DB_PREFIX_ . 'customer'] = Db::getInstance()->executeS(
                    'SHOW columns FROM `' . _DB_PREFIX_ . 'customer` WHERE `Field` NOT LIKE "id_%"'
                );
                $attributes_available[_DB_PREFIX_ . 'gender_lang'] = Db::getInstance()->executeS(
                    'SHOW columns FROM `' . _DB_PREFIX_ . 'gender_lang` WHERE `Field` NOT LIKE "id_%"'
                );
            } elseif ($config['tab'] == 'address') {
                $attributes_available[_DB_PREFIX_ . 'address'] = Db::getInstance()->executeS(
                    'SHOW columns FROM `' . _DB_PREFIX_ . 'address` WHERE `Field` NOT LIKE "id_%"'
                );
                $attributes_available[_DB_PREFIX_ . 'customer'] = Db::getInstance()->executeS(
                    'SHOW columns FROM `' . _DB_PREFIX_ . 'customer` WHERE `Field` NOT LIKE "id_%"'
                );
                $attributes_available[_DB_PREFIX_ . 'country'] = Db::getInstance()->executeS(
                    'SHOW columns FROM `' . _DB_PREFIX_ . 'country` WHERE `Field` NOT LIKE "id_%"'
                );
                $attributes_available[_DB_PREFIX_ . 'country_lang'] = Db::getInstance()->executeS(
                    'SHOW columns FROM `' . _DB_PREFIX_ . 'country_lang` WHERE `Field` NOT LIKE "id_%"'
                );
            } elseif ($config['tab'] == 'cart') {
                $attributes_available[_DB_PREFIX_ . 'cart'] = Db::getInstance()->executeS(
                    'SHOW columns FROM `' . _DB_PREFIX_ . 'cart` WHERE `Field` NOT LIKE "id_%"'
                );
                $attributes_available[_DB_PREFIX_ . 'customer'] = Db::getInstance()->executeS(
                    'SHOW columns FROM `' . _DB_PREFIX_ . 'customer` WHERE `Field` NOT LIKE "id_%"'
                );
                $attributes_available[_DB_PREFIX_ . 'carrier'] = Db::getInstance()->executeS(
                    'SHOW columns FROM `' . _DB_PREFIX_ . 'carrier` WHERE `Field` NOT LIKE "id_%"'
                );
                $attributes_available[_DB_PREFIX_ . 'orders'] = Db::getInstance()->executeS(
                    'SHOW columns FROM `' . _DB_PREFIX_ . 'orders` WHERE `Field` NOT LIKE "id_%"'
                );
            } elseif ($config['tab'] == 'customer_thread') {
                $attributes_available[_DB_PREFIX_ . 'customer_thread'] = Db::getInstance()->executeS(
                    'SHOW columns FROM `' . _DB_PREFIX_ . 'customer_thread` WHERE Fie`ld NO`T LIKE "id_%"'
                );
                $attributes_available[_DB_PREFIX_ . 'customer'] = Db::getInstance()->executeS(
                    'SHOW columns FROM `' . _DB_PREFIX_ . 'customer` WHERE `Field` NOT LIKE "id_%"'
                );
                $attributes_available[_DB_PREFIX_ . 'customer_message'] = Db::getInstance()->executeS(
                    'SHOW columns FROM `' . _DB_PREFIX_ . 'customer_message` WHERE `Field` NOT LIKE "id_%"'
                );
                $attributes_available[_DB_PREFIX_ . 'contact_lang'] = Db::getInstance()->executeS(
                    'SHOW columns FROM `' . _DB_PREFIX_ . 'contact_lang` WHERE `Field` NOT LIKE "id_%"'
                );
            }
        }

        $this->context->smarty->assign(array(
            'id_extracolumns' => $this->context->cookie->ec_list,
            'modules' => $listModules,
            'selected_table' => $selected_table,
            'prefix' => _DB_PREFIX_,
            'refresh' => 'index.php?controller=' . $this->tabClassName . '&add' . $this->table .
                '&token=' . Tools::getAdminTokenLite('AdminExtraColumns') . '&refresh=1',
            'attributes_available' => $attributes_available,
            'link_to_module' => 'index.php?controller=' . $this->tabClassName .
                '&token=' . Tools::getAdminTokenLite('AdminExtraColumns'),
            'positions' => $positions,
            'replace' => $replace,
            'types' => $this->types,
            'default_list' => $this->getDefaultList($config ['tab'])
        ));

        $admin_form = $this->context->smarty->fetch($this->module->getLocalPath() .
        'views/templates/admin/admin_form/available_tables.tpl');
        $admin_form .= $this->context->smarty->fetch($this->module->getLocalPath() .
        'views/templates/admin/admin_form/available_attributes.tpl');
        $admin_form .= $this->context->smarty->fetch($this->module->getLocalPath() .
        'views/templates/admin/admin_form/position_attributes.tpl');
        $script = $this->context->smarty->fetch($this->module->getLocalPath() .
          'views/templates/admin/admin_form/configure.tpl');

        return $admin_form . $script;
    }

    public function ajaxProcessSendInputList()
    {
        // AJAX DATAS :
        // EC_input
        // id_extracolumns_columns
        // attribute

        Db::getInstance()->execute(
            "UPDATE `" . _DB_PREFIX_ . $this->table . "`
            SET `" . pSQL(Tools::getValue('attribute')) . "` = '" . pSQL(Tools::getValue('EC_input')) . "'
            WHERE `id_extracolumns_columns` = '" . (int)Tools::getValue('id_extracolumns_columns') . "'"
        );

        die(true);
    }

    public function processChangeProfile()
    {
        $tab = Tools::getValue('ec_list');

        switch ($tab) {
            case 'orders':
                $controller = 'AdminOrders';
                break;
            case 'address':
                $controller = 'AdminAddresses';
                break;
            case 'cart':
                $controller = 'AdminCarts';
                break;
            case 'customer':
                $controller = 'AdminCustomers';
                break;
            case 'customer_thread':
                $controller = 'AdminCustomerThreads';
                break;
        }

        if (isset($controller)) {
            ExtraColumnsProfile::updateProfileForEmployee(
                $tab,
                $this->context->employee->id,
                (int)Tools::getValue('id_profile')
            );

            $link = $this->context->link->getAdminLink($controller);

            Tools::redirectAdmin($link);
        }

        return false;
    }

    public function processSaveTemplate()
    {
        if (Tools::isSubmit('submitSaveOn')) {
            $profile = new ExtraColumnsProfile((int)Tools::getValue('id_profile'));
            if (Validate::isLoadedObject($profile)) {
                Db::getInstance()->execute(
                    "DELETE FROM `" . _DB_PREFIX_ . "extracolumns_columns`
                    WHERE `id_extracolumns_profiles` = " . (int)$profile->id
                );
                if (!Db::getInstance()->execute(
                    "INSERT INTO `" . _DB_PREFIX_ . "extracolumns_columns`
                    (`id_extracolumns`, `id_extracolumns_profiles`, `position`, `table`, `attribute`, `title`, `type`)
                    SELECT `id_extracolumns`, ".(int)$profile->id.", `position`, `table`, `attribute`, `title`, `type`
                    FROM `" . _DB_PREFIX_ . "extracolumns_columns`
                    WHERE `id_extracolumns_profiles` = 1
                    AND `id_extracolumns` = " . (int)$this->context->cookie->ec_list
                )) {
                    $this->errors[] = $this->l('An error has occurred.');
                } else {
                    Db::getInstance()->execute(
                        "UPDATE `" . _DB_PREFIX_ . "extracolumns_profiles`
                        SET `replace` = (
                            SELECT `replace`
                            FROM `" . _DB_PREFIX_ . "extracolumns`
                            WHERE `id_extracolumns` = " . (int)$this->context->cookie->ec_list . "
                        ), `start` = (
                            SELECT `start`
                            FROM `" . _DB_PREFIX_ . "extracolumns`
                            WHERE `id_extracolumns` = " . (int)$this->context->cookie->ec_list . "
                        ) WHERE `id_extracolumns_profiles` = " . $profile->id . "
                        AND `id_extracolumns` = " . (int)$this->context->cookie->ec_list
                    );
                    Tools::redirectAdmin($this->context->link->getAdminLink('AdminExtraColumns') . '&conf=4');
                }
            } else {
                $this->errors[] = $this->l('This profile does not exist.');
            }
        } elseif (Tools::isSubmit('submitSaveAs')) {
            $profile = new ExtraColumnsProfile();
            $profile->name = Tools::getValue('name');
            $profile->id_extracolumns = (int)$this->context->cookie->ec_list;
            if ($profile->add()) {
                if (!Db::getInstance()->execute(
                    "INSERT INTO `" . _DB_PREFIX_ . "extracolumns_columns`
                    (`id_extracolumns`, `id_extracolumns_profiles`, `position`, `table`, `attribute`, `title`, `type`)
                    SELECT `id_extracolumns`, ".(int)$profile->id.", `position`, `table`, `attribute`, `title`, `type`
                    FROM `" . _DB_PREFIX_ . "extracolumns_columns`
                    WHERE `id_extracolumns_profiles` = 1
                    AND `id_extracolumns` = " . (int)$this->context->cookie->ec_list
                )) {
                    $this->errors[] = $this->l('An error has occurred.');
                } else {
                    Db::getInstance()->execute(
                        "UPDATE `" . _DB_PREFIX_ . "extracolumns_profiles`
                        SET `replace` = (
                            SELECT `replace`
                            FROM `" . _DB_PREFIX_ . "extracolumns`
                            WHERE `id_extracolumns` = " . (int)$this->context->cookie->ec_list . "
                        ), `start` = (
                            SELECT `start`
                            FROM `" . _DB_PREFIX_ . "extracolumns`
                            WHERE `id_extracolumns` = " . (int)$this->context->cookie->ec_list . "
                        ) WHERE `id_extracolumns_profiles` = " . $profile->id . "
                        AND `id_extracolumns` = " . (int)$this->context->cookie->ec_list
                    );
                    Tools::redirectAdmin($this->context->link->getAdminLink('AdminExtraColumns') . '&conf=3');
                }
            } else {
                $this->errors[] = $this->l('An error has occurred.');
            }
        }
    }

    public function processManageTemplate()
    {
        if (Tools::isSubmit('submitDeleteTemplate')) {
            $profile = new ExtraColumnsProfile((int)Tools::getValue('id_profile'));
            if (Validate::isLoadedObject($profile)) {
                if (!$profile->delete()) {
                    $this->errors[] = $this->l('An error has occurred.');
                } else {
                    Tools::redirectAdmin($this->context->link->getAdminLink('AdminExtraColumns') . '&conf=1');
                }
            } else {
                $this->errors[] = $this->l('This profile does not exist.');
            }
        } elseif (Tools::isSubmit('submitEditTemplate')) {
            $profile = new ExtraColumnsProfile((int)Tools::getValue('id_profile'));
            if (Validate::isLoadedObject($profile)) {
                $profile->name = Tools::getValue('name');
                try {
                    $profile->update();
                    Tools::redirectAdmin($this->context->link->getAdminLink('AdminExtraColumns') . '&conf=4');
                } catch (Exception $e) {
                    $this->errors[] = $this->l('Please fill the name field.');
                }
            } else {
                $this->errors[] = $this->l('This profile does not exist.');
            }
        }
    }

    public function processLoadTemplate()
    {
        if ($this->context->cookie->ec_list &&
            ExtraColumnsProfile::existsInDatabase(Tools::getValue('id_profile'), 'extracolumns_profiles')
        ) {
            Db::getInstance()->execute(
                "DELETE FROM `" . _DB_PREFIX_ . "extracolumns_columns` WHERE `id_extracolumns_profiles` = 1"
            );
            Db::getInstance()->execute(
                "INSERT INTO `" . _DB_PREFIX_ . "extracolumns_columns`
                (`id_extracolumns`, `id_extracolumns_profiles`, `position`, `table`, `attribute`, `title`, `type`)
                SELECT `id_extracolumns`, 1, `position`, `table`, `attribute`, `title`, `type`
                FROM `" . _DB_PREFIX_ . "extracolumns_columns`
                WHERE `id_extracolumns` = " . (int)$this->context->cookie->ec_list . "
                AND `id_extracolumns_profiles` = " . (int)Tools::getValue('id_profile')
            );
            Db::getInstance()->execute(
                "UPDATE `" . _DB_PREFIX_ . "extracolumns`
                SET `replace` = (
                    SELECT `replace`
                    FROM `" . _DB_PREFIX_ . "extracolumns_profiles`
                    WHERE `id_extracolumns_profiles` = " . (int)Tools::getValue('id_profile') . "
                ) WHERE `id_extracolumns` = " . (int)$this->context->cookie->ec_list
            );

            if (version_compare(_PS_VERSION_, '1.7.6.0', '<')) {
                $link = $this->context->link->getAdminLink('AdminExtraColumns', true) .
                    '&conf=4';
            } else {
                $link = $this->context->link->getAdminLink(
                    'AdminExtraColumns',
                    true,
                    [],
                    ['conf' => 4]
                );
            }
            Tools::redirectAdmin($link);
        } else {
            $this->errors[] = $this->l('This template does not exist');
        }
    }

    public function getDefaultList($list = 'orders')
    {
        $list = ($list == 'orders' ? 'order' : $list);
        // $this->l('order')
        // $this->l('cart')
        return array(
            //EXTRACOLUMNS
            'extracolumns-customer' => array(
                'description' => $this->l('Customer\'s name'),
                'title' => $this->l('Customer'),
                'type' => false
            ),
            'extracolumns-new_client' => array(
                'description' => $this->l('New client'),
                'title' => $this->l('New client'),
                'type' => 'boolean'
            ),
            'extracolumns-total_spent' => array(
                'description' => $this->l('Total spent'),
                'title' => $this->l('Sales'),
                'type' => 'price'
            ),
            'extracolumns-last_visit' => array(
                'description' => $this->l('Last visit'),
                'title' => $this->l('Last visit'),
                'type' => 'datetime'
            ),
            'extracolumns-nb_orders' => array(
                'description' => $this->l('The number of orders'),
                'title' => $this->l('Nb orders'),
                'type' => false
            ),
            'extracolumns-default_group' => array(
                'description' => $this->l('Default group'),
                'title' => $this->l('Default group'),
                'type' => false
            ),
            'extracolumns-sum_discount' => array(
                'description' => $this->l('Discount amount'),
                'title' => $this->l('Discount'),
                'type' => 'price'
            ),
            'extracolumns-invoice_name' => array(
                'description' => $this->l('Invoice name'),
                'title' => $this->l('Invoice'),
                'type' => false
            ),

            //ORDERS
            'orders-reference' => array(
                'description' => $this->l('Order Reference'),
                'title' => $this->l('Reference'),
                'type' => false,
            ),
            'orders-payment' => array(
                'description' => $this->l('Current state of the payment'),
                'title' => $this->l('Payment'),
                'type' => 'select',
            ),
            'orders-recyclable' => array(
                'description' => $this->l('Is this recyclable?'),
                'type' => 'boolean',
            ),
            'orders-gift' => array(
                'description' => $this->l('Is this a gift?'),
                'type' => 'boolean',
            ),
            'orders-giftmessage' => array(
                'description' => $this->l('Gift message'),
                'type' => false,
            ),
            'orders-shipping_number' => array(
                'description' => $this->l('Shipping number') .
                    " (" . $this->l('deprecated, use this instead:') .
                    " table:order_carrier ; attribute:tracking_number)",
                'title' => $this->l('Shipping number'),
                'type' => false,
            ),
            'orders-total_discounts' => array(
                'description' => $this->l('Total amount of discounts'),
                'type' => 'price',
            ),
            'orders-total_discounts_tax_incl' => array(
                'description' => $this->l('Total amount of discounts with taxes'),
                'type' => 'price',
            ),
            'orders-total_discounts_tax_excl' => array(
                'description' => $this->l('Total amount of discounts without taxes'),
                'type' => 'price',
            ),
            'orders-total_paid' => array(
                'description' => $this->l('Total amount paid'),
                'title' => $this->l('Total'),
                'type' => 'price',
            ),
            'orders-total_paid_tax_incl' => array(
                'description' => $this->l('Total amount paid with taxes'),
                'title' => $this->l('Total'),
                'type' => 'price',
            ),
            'orders-total_paid_tax_excl' => array(
                'description' => $this->l('Total amount paid without taxes'),
                'title' => $this->l('Total without taxes'),
                'type' => 'price',
            ),
            'orders-total_products' => array(
                'description' => $this->l('Total amount of products'),
                'type' => 'price',
            ),
            'orders-total_shipping' => array(
                'description' => $this->l('Total amount of shipping'),
                'type' => 'price',
            ),
            'orders-invoice_number' => array(
                'description' => $this->l('Invoice number'),
                'title' => $this->l('Invoice'),
                'type' => false,
            ),
            'orders-delivery_number' => array(
                'description' => $this->l('Delivery number'),
                'type' => false,
            ),
            'orders-invoice_date' => array(
                'description' => $this->l('Invoice date'),
                'type' => 'datetime',
            ),
            'orders-delivery_date' => array(
                'description' => $this->l('Delivery date'),
                'type' => 'datetime',
            ),
            'orders-date_add' => array(
                'description' => $this->l('Date when the order was made'),
                'title' => $this->l('Date'),
                'type' => 'datetime',
            ),
            'orders-date_upd' => array(
                'description' => $this->l('Date when the order was updated'),
                'type' => 'date',
            ),
            'orders-current_state' => array(
                'description' => version_compare(_PS_VERSION_, '1.7.7.0', '>=') ? $this->l('Order state') : '',
                'title' => $this->l('Status'),
                'type' => false,
            ),

            //CUSTOMER
            'customer-company' => array(
                'description' => $this->l('Company name'),
                'title' => $this->l('Company'),
                'type' => false,
            ),
            'customer-siret' => array(
                'description' => $this->l('Siret number'),
                'title' => $this->l('Siret'),
                'type' => false,
            ),
            'customer-ape' => array(
                'description' => $this->l('APE number'),
                'title' => $this->l('APE'),
                'type' => false,
            ),
            'customer-firstname' => array(
                'description' => $this->l('Firstname'),
                'title' => $this->l('Firstname'),
                'type' => false,
            ),
            'customer-lastname' => array(
                'description' => $this->l('Lastname'),
                'title' => $this->l('Lastname'),
                'type' => false,
            ),
            'customer-email' => array(
                'description' => $this->l('E-mail'),
                'title' => $this->l('E-mail'),
                'type' => false,
            ),
            'customer-birthday' => array(
                'description' => $this->l('Birthday'),
                'type' => 'date',
            ),
            'customer-newsletter' => array(
                'description' => $this->l('Is he subscribed to the newsletter?'),
                'title' => $this->l('Newsletter'),
                'type' => 'boolean',
            ),
            'customer-optin' => array(
                'description' => $this->l('Is he subscribed to the partners offers?'),
                'title' => $this->l('Partners offers'),
                'type' => 'boolean',
            ),
            'customer-website' => array(
                'description' => $this->l('Website'),
                'type' => false,
            ),
            'customer-note' => array(
                'description' => $this->l('Note you write about him'),
                'title' => $this->l('Note'),
                'type' => false,
            ),
            'customer-is_guest' => array(
                'description' => $this->l('Is he a guest?'),
                'type' => 'bool',
            ),
            'customer-date_add' => array(
                'description' => $this->l('Date of registration'),
                'title' => $this->l('Registration'),
                'type' => 'datetime',
            ),
            'customer-date_upd' => array(
                'description' => $this->l('Date of last connection'),
                'type' => 'datetime',
            ),
            'customer-active' => array(
                'description' => $this->l('Enable or disable customer login'),
                'title' => $this->l('Enabled'),
                'type' => 'boolean',
            ),

            //ADDRESS
            'address-alias' => array(
                'description' => $this->l('Alias'),
                'title' => $this->l('Alias'),
                'type' => false,
            ),
            'address-company' => array(
                'description' => $this->l('Company') . ($list == 'order' ? ' (' . $this->l('delivery') . ')' : ''),
                'title' => $this->l('Company'),
                'type' => false,
            ),
            'address-lastname' => array(
                'description' => $this->l('Lastname') . $this->l(' used for this address'),
                'title' => $this->l('Lastname'),
                'type' => false,
            ),
            'address-firstname' => array(
                'description' => $this->l('Firstname') . $this->l(' used for this address'),
                'title' => $this->l('Firstname'),
                'type' => false,
            ),
            'address-address1' => array(
                'description' => $this->l('Address') . ($list == 'order' ? ' (' . $this->l('delivery') . ')' : ''),
                'title' => $this->l('Address'),
                'type' => false,
            ),
            'address-address2' => array(
                'description' => $this->l('Address Complement') .
                    ($list == 'order' ? ' (' . $this->l('delivery') . ')' : ''),
                'title' => $this->l('Complement'),
                'type' => false,
            ),
            'address-postcode' => array(
                'description' => $this->l('Postcode'),
                'title' => $this->l('Postcode'),
                'type' => false,
            ),
            'address-city' => array(
                'description' => $this->l('City'),
                'title' => $this->l('City'),
                'type' => false,
            ),
            'address-other' => array(
                'description' => $this->l('Other'),
                'type' => false,
            ),
            'address-phone' => array(
                'description' => $this->l('Phone number') . $this->l(' used for this address'),
                'title' => $this->l('Phone'),
                'type' => false,
            ),
            'address-vat_number' => array(
                'description' => $this->l('VAT identification number'),
                'type' => false,
            ),

            //BILLING_ADDRESS
            'billing_address-company' => array(
                'description' => $this->l('Company') . ' (' . $this->l('billing') . ')',
                'title' => $this->l('Company'),
                'type' => false,
            ),
            'billing_address-lastname' => array(
                'description' => $this->l('Lastname'),
                'title' => $this->l('Lastname'),
                'type' => false,
            ),
            'billing_address-firstname' => array(
                'description' => $this->l('Firstname'),
                'title' => $this->l('Firstname'),
                'type' => false,
            ),
            'billing_address-address1' => array(
                'description' => $this->l('Address') . ' (' . $this->l('billing') . ')',
                'title' => $this->l('Address'),
                'type' => false,
            ),
            'billing_address-address2' => array(
                'description' => $this->l('Address Complement') . ' (' . $this->l('billing') . ')',
                'title' => $this->l('Complement'),
                'type' => false,
            ),

            //COUNTRY
            'country-iso_code' => array(
                'description' => $this->l('ISO code'),
                'type' => false,
            ),
            'country-call_prefix' => array(
                'description' => $this->l('Prefix to phone number'),
                'type' => false,
            ),

            //COUNTRY_LANG
            'country_lang-name' => array(
                'description' => $this->l('Country name'),
                'title' => $this->l('Country'),
                'type' => 'select',
            ),

            //ORDER_STATE
            'order_state-shipped' => array(
                'description' => $this->l('Is shipped?'),
                'type' => 'bool',
            ),
            'order_state-paid' => array(
                'description' => $this->l('Is paid?'),
                'type' => 'bool',
            ),
            //ORDER_STATE_LANG
            'order_state_lang-name' => array(
                'description' => version_compare(_PS_VERSION_, '1.7.7.0', '<') ? $this->l('Order State') : '',
                'title' => $this->l('Status'),
                'type' => 'select',
            ),

            //ORDER_PAYMENT
            'order_payment-payment_method' => array(
                'description' => $this->l('Payment history'),
                'title' => $this->l('Payment history'),
                'type' => false,
            ),

            //ORDER_CART_RULE
            'order_cart_rule-name' => array(
                'description' => $this->l('Discount name') . ' (' . $this->l('Cart rule') . ')',
                'title' => $this->l('Discount'),
                'type' => false,
            ),
            'order_cart_rule-value' => array(
                'description' => $this->l('Discount amount') . ' (' . $this->l('Cart rule') . ')',
                'title' => $this->l('Discount amount'),
                'type' => 'price',
            ),
            'order_detail-product_weight' => array(
                'description' => $this->l('Total weight'),
                'title' => $this->l('Weight'),
                'type' => false,
            ),

            //CART
            'cart-recyclable' => array(
                'description' => $this->l('Is this recyclable?'),
                'type' => 'boolean',
            ),
            'cart-gift' => array(
                'description' => $this->l('Is this a gift?'),
                'type' => 'boolean',
            ),
            'cart-giftmessage' => array(
                'description' => $this->l('Gift message'),
                'type' => false,
            ),
            'cart-date_add' => array(
                'description' => $this->l('Date when the cart was created'),
                'type' => 'date',
            ),
            'cart-date_upd' => array(
                'description' => $this->l('Date when the cart was updated'),
                'title' => $this->l('Date'),
                'type' => 'date',
            ),
            'cart-total' => array(
                'description' => $this->l('Total amount of the cart'),
                'title' => $this->l('Total'),
                'type' => false,
            ),
            'cart-status' => array(
                'description' => $this->l('Order ID'),
                'title' => $this->l('Order ID'),
                'type' => false,
            ),

            //CART_RULE
            'cart_rule-code' => array(
                'description' => $this->l('Discount code'),
                'title' => $this->l('Discount code'),
                'type' => false,
            ),

            //CARRIER
            'carrier-name' => array(
                'description' => $this->l('Carrier name'),
                'title' => $this->l('Carrier'),
                'type' => false,
            ),
            'carrier-url' => array(
                'description' => 'URL',
                'type' => false,
            ),

            //ORDER_CARRIER
            'order_carrier-tracking_number' => array(
                'description' => $this->l('Tracking number'),
                'title' => $this->l('Tracking number'),
                'type' => false,
            ),

            //CUSTOMER_THREAD
            'customer_thread-status' => array(
                'description' => $this->l('Status'),
                'title' => $this->l('Status'),
                'type' => false,
            ),
            'customer_thread-email' => array(
                'description' => $this->l('E-mail'),
                'title' => $this->l('E-mail'),
                'type' => false,
            ),
            'customer_thread-date_add' => array(
                'description' => $this->l('Date when the thread was created'),
                'type' => 'date',
            ),
            'customer_thread-date_upd' => array(
                'description' => $this->l('Date when the thread was updated'),
                'title' => $this->l('Last message'),
                'type' => 'date',
            ),

            //CUSTOMER_MESSAGE
            'customer_message-message' => array(
                'description' => $this->l('Message'),
                'title' => $this->l('Message'),
                'type' => false,
            ),

            //CONTACT_LANG
            'contact_lang-name' => array(
                'description' => $this->l('Contact name (Webmaster, ...)'),
                'title' => $this->l('Contact'),
                'type' => false,
            ),

            //PRODUCT
            'product-ean13' => array(
                'description' => 'EAN-13 ' . sprintf($this->l(' of all the products of the %s'), $this->l($list)),
                'title' => 'EAN-13',
                'type' => false,
            ),
            'product-isbn' => array(
                'description' => 'ISBN ' . sprintf($this->l(' of all the products of the %s'), $this->l($list)),
                'title' => 'ISBN',
                'type' => false,
            ),
            'product-upc' => array(
                'description' => 'UPC ' . sprintf($this->l(' of all the products of the %s'), $this->l($list)),
                'title' => 'UPC',
                'type' => false,
            ),
            'product-ecotax' => array(
                'description' => $this->l('Ecotax') . ' ' .
                    sprintf($this->l(' of all the products of the %s'), $this->l($list)),
                'type' => false,
            ),
            'product-minimal_quantity' => array(
                'description' => $this->l('Minimal quantity') . ' ' .
                    sprintf($this->l(' of all the products of the %s'), $this->l($list)),
                'type' => false,
            ),
            'product-low_stock_alert' => array(
                'description' => $this->l('Low stock alert') . ' ' .
                    sprintf($this->l(' of all the products of the %s'), $this->l($list)),
                'type' => false,
            ),
            'product-price' => array(
                'description' => $this->l('Price') . ' ' .
                    sprintf($this->l(' of all the products of the %s'), $this->l($list)),
                'type' => 'price',
            ),
            'product-wholesale_price' => array(
                'description' => $this->l('Wholesale price') . ' ' .
                    sprintf($this->l(' of all the products of the %s'), $this->l($list)),
                'type' => 'price',
            ),
            'product-additional_shipping_cost' => array(
                'description' => $this->l('Additional shipping cost') . ' ' .
                    sprintf($this->l(' of all the products of the %s'), $this->l($list)),
                'type' => 'price',
            ),
            'product-reference' => array(
                'description' => $this->l('Reference') . ' ' .
                    sprintf($this->l(' of all the products of the %s'), $this->l($list)),
                'title' => $this->l('Products'),
                'type' => false,
            ),
            'product-supplier_reference' => array(
                'description' => $this->l('Supplier reference') . ' ' .
                    sprintf($this->l(' of all the products of the %s'), $this->l($list)),
                'type' => false,
            ),
            'product-location' => array(
                'description' => $this->l('Location') . ' ' .
                    sprintf($this->l(' of all the products of the %s'), $this->l($list)),
                'title' => $this->l('Location'),
                'type' => false,
            ),
            'product-width' => array(
                'description' => $this->l('Width') . ' ' .
                    sprintf($this->l(' of all the products of the %s'), $this->l($list)),
                'type' => false,
            ),
            'product-height' => array(
                'description' => $this->l('Height') . ' ' .
                    sprintf($this->l(' of all the products of the %s'), $this->l($list)),
                'type' => false,
            ),
            'product-depth' => array(
                'description' => $this->l('Depth') . ' ' .
                    sprintf($this->l(' of all the products of the %s'), $this->l($list)),
                'type' => false,
            ),
            'product-weight' => array(
                'description' => $this->l('Weight') . ' ' .
                    sprintf($this->l(' of all the products of the %s'), $this->l($list)),
                'type' => false,
            ),

            //PRODUCT_LANG
            'product_lang-name' => array(
                'description' => $this->l('Name') . ' ' .
                    sprintf($this->l(' of all the products of the %s'), $this->l($list)),
                'title' => $this->l('Products'),
                'type' => false,
            ),

            //CUSTOMIZED_DATA
            'customized_data-value' => array(
                'description' => $this->l('Customized texts'),
                'title' => $this->l('Customized texts'),
                'type' => false,
            ),

            //LANG
            'lang-name' => array(
                'description' => $this->l('Language used'),
                'title' => $this->l('Language'),
                'type' => false,
            ),

            //GENDER_LANG
            'gender_lang-name' => array(
                'description' => $this->l('Social title'),
                'title' => $this->l('Social title'),
                'type' => 'select',
            ),

            //GROUP_LANG
            'group_lang-name' => array(
                'description' => $this->l('Groups'),
                'title' => $this->l('Groups'),
                'type' => false
            ),

            //SUPPLIER
            'supplier-name' => array(
                'description' => $this->l('Suppliers') . ' ' .
                    sprintf($this->l(' of all the products of the %s'), $this->l($list)),
                'title' => $this->l('Suppliers'),
                'type' => false
            ),

            //IMAGE
            'image-id_image' => array(
                'description' => $this->l('Products thumbnails'),
                'title' => $this->l('Products'),
                'type' => false
            )
        );
    }
}
