{*
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<div class="container">
	<div class="row">
		<div class="panel">
			<form class="form form-horizontal" method="POST">
				<div class="panel-heading"><i class="icon-cogs"></i> {l s='Quick add' mod='extracolumns'}</div>
				<div class="panel-body">
					<div class="row">
						<label class="control-label col-lg-5">
							{l s='Quick add' mod='extracolumns'}:
						</label>
						<div class="col-lg-3">
							<div class="col-lg-12">
								<select name="EXTRACOLUMNS_QUICK_ADD" class="form-control" id="extracolumns_quick_add">
									{foreach from=$quick_add item=category_array key=category_name}
									<optgroup label="{$category_name|escape:'htmlall':'UTF-8'}">
										{foreach from=$category_array item=column_key}
											<option value="{$column_key|escape:'htmlall':'UTF-8'}">{$description[$column_key]['description']|escape:'htmlall':'UTF-8'}</option>
										{/foreach}
										{/foreach}
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-5"></div>
						<div class="col-lg-7">
							<p class="help-block">{l s='If you don\'t find the attribute you want to add, please click on the button "Add a new column" at the top right of the page' mod='extracolumns'}</p>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<input type="hidden" name="id_extracolumns" value="{$id_extracolumns|escape:'htmlall':'UTF-8'}">
					<button type="submit" value="1" name="submitQuickAdd" class="btn btn-default pull-right">
						<i class="process-icon-save"></i>
						{l s='Add' mod='extracolumns'}
					</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="save_template-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        	<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="{l s='Close' mod='extracolumns'}">
					<span aria-hidden="true">&times;</span>
				</button>
	        	<h3 class="modal-title">{l s='Save as template' mod='extracolumns'}</h3>
      		</div>
      		<div class="modal-body">
				<form class="" action="{$ec_link->getAdminLink('AdminExtraColumns')|escape:'htmlall':'UTF-8'}&action=save_template" method="post">
					{if !empty($profiles)}
						<h4>{l s='Modify an existing template' mod='extracolumns'}</h4>
						<br>
						<div class="form-group">
						    <label for="profiles-select">{l s='Save on' mod='extracolumns'}</label>
						    <select class="form-control" id="profiles-select" name="id_profile">
								{foreach from=$profiles item=profile}
							      	<option value="{$profile.id_extracolumns_profiles|intval}">{$profile.name|escape:'htmlall':'UTF-8'}</option>
								{/foreach}
						    </select>
							<p class="help-block">{l s='This will erase the previous configuration by the new one' mod='extracolumns'}</p>
					    </div>

						<button class="btn btn-primary" type="submit" value="1" name="submitSaveOn">{l s='Save' mod='extracolumns'}</button>

						<hr>
					{/if}

					<h4>{l s='Add a new template' mod='extracolumns'}</h4>
					<br>

					<div class="form-group">
		      			<label for="template-input">{l s='Name of the new template' mod='extracolumns'}</label>
		      			<input name="name" type="text" class="form-control" id="template-input" placeholder="Name">
		    		</div>

					<button class="btn btn-primary" type="submit" value="1" name="submitSaveAs">{l s='Create' mod='extracolumns'}</button>
				</form>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-secondary" data-dismiss="modal">{l s='Close' mod='extracolumns'}</button>
      		</div>
    	</div>
  	</div>
</div>

<div class="modal fade" id="manage_templates-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="manage_templates-form" action="{$ec_link->getAdminLink('AdminExtraColumns')|escape:'htmlall':'UTF-8'}&action=manage_template" method="post">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="{l s='Close' mod='extracolumns'}"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">{l s='Manage templates' mod='extracolumns'}</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="manage_templates-input">{l s='Select the template to manage' mod='extracolumns'}</label>
						<select class="form-control form-control-sm" id="manage_templates-select" name="id_profile">
							<option value="0"></option>
							{foreach from=$profiles item=profile}
								<option value="{$profile.id_extracolumns_profiles|intval}">{$profile.name|escape:'htmlall':'UTF-8'}</option>
							{/foreach}
						</select>
					</div>
					<div class="manage_templates-to_hide">
						<div class="form-group">
							<label for="manage_templates-input">{l s='Change the name' mod='extracolumns'}</label>
							<input class="form-control form-control-sm" name="name" id="manage_templates-input" />
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">{l s='Close' mod='extracolumns'}</button>
					<button name="submitDeleteTemplate" value="1" type="submit" id="manage_templates-delete_btn" class="btn btn-danger manage_templates-to_hide">{l s='Delete' mod='extracolumns'}</button>
					<button name="submitEditTemplate" value="1" type="submit" id="manage_templates-save_btn" class="btn btn-primary manage_templates-to_hide">{l s='Save changes' mod='extracolumns'}</button>
				</div>
			</form>
		</div>
	</div>
	<!-- <script type="text/javascript">
		$(document).ready(function() {
			$('.manage_templates-to_hide').hide();
			$('#manage_templates-select').on('change', function() {
				if ($(this).val() == 0) {
					$('.manage_templates-to_hide').hide();
				} else {
					$('.manage_templates-to_hide').show();
					$('#manage_templates-input').val($(this).find('option[value="' + $(this).val() +'"]').text());
				}
			});
			$('#manage_templates-save_btn').on('click', function(e) {
				$.post('/Report/ajaxEditTemplate' + "?" + $('#form-tabs').serialize(), {
					id: $('#manage_templates-select').val(),
					name: $('#manage_templates-input').val()
				}, ajaxCallback);
				e.preventDefault();
			});
			$('#manage_templates-delete_btn').on('click', function(e) {
				$.post('/Report/ajaxDeleteTemplate' + "?" + $('#form-tabs').serialize(), {
					id: $('#template_edit-select').val()
				}, ajaxCallback);
				e.preventDefault();
			});
		});
	</script> -->
</div>
