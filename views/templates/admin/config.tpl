{*
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="row">
    <div class="col-md-12">
    	<div class="panel container">
    		<form class="form form-horizontal" method="POST">
    			<div class="panel-heading"><i class="icon-cogs"></i> {l s='General' mod='extracolumns'}</div>
    			<div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-lg-5">
        					{l s='Replace : ' mod='extracolumns'}
        				</label>
        				<div class="col-lg-7">
        					<span class="switch prestashop-switch fixed-width-lg">
        						<input type="radio" name="replace" id="replace_on" value="1" {if $replace}checked="checked"{/if}>
        						<label for="replace_on">{l s='Yes' mod='extracolumns'}</label>
        						<input type="radio" name="replace" id="replace_off" value="" {if !$replace}checked="checked"{/if}>
        						<label for="replace_off">{l s='No' mod='extracolumns'}</label>
        						<a class="slide-button btn"></a>
        					</span>
        					<p class="help-block">{l s='If Yes, all the selectionned attributes will be replaced the actual tab.' mod='extracolumns'}<br>
        						{l s='If No, the attributes will be displayed after the actual tab.' mod='extracolumns'}
        					</p>
        				</div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-5">
							{l s='All columns in the list above will start:' mod='extracolumns'}:
						</label>
						<div class="col-lg-3">
							<div class="col-lg-12">
								<select name="start" class="form-control">
                                    <option value="id"{if $start=='id'} selected{/if}>{l s='after the ID' mod='extracolumns'}</option>
									<option value="action"{if $start=='action'} selected{/if}>{l s='before the action buttons' mod='extracolumns'}</option>
								</select>
							</div>
						</div>
                    </div>
    			</div>
    			<div class="panel-footer">
    				<input type="hidden" name="id_extracolumns" value="{$id_extracolumns|intval}">
    				<button type="submit" value="1" name="submitGlobalForm" class="btn btn-default pull-right">
    					<i class="process-icon-save"></i>
    					{l s='Validate' mod='extracolumns'}
    				</button>
    			</div>
    		</form>
    	</div>
    </div>
</div>
