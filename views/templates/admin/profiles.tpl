{*
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if !empty($profiles) and !empty($form_name)}
    <script>
        $('document').ready(function() {
            let profiles_dropdown = '<a id="desc-order-extracolumns" class="list-toolbar-btn" href="#" data-toggle="dropdown" onclick="return false;"><span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="{l s='Load another template' mod='extracolumns'}" data-html="true" data-placement="top"><i class="process-icon-configure"></i></span></a>';

            profiles_dropdown += '<ul class="dropdown-menu">';
            {foreach from=$profiles item=profile}
                profiles_dropdown += '<li><a href="{$ec_link->getAdminLink('AdminExtraColumns')|escape:'htmlall':'UTF-8'}&action=change_profile&ec_list={$ec_list|escape:'htmlall':'UTF-8'}&id_profile={$profile.id_extracolumns_profiles|intval}">{$profile.name|escape:'htmlall':'UTF-8'}</a></li>';
            {/foreach}
            profiles_dropdown += '</ul>';

            $('#{$form_name|escape:'htmlall':'UTF-8'} .panel-heading-action').prepend(profiles_dropdown);
        });
    </script>
{/if}
