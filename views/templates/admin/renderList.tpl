{*
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<script type="text/javascript">
$('document').ready(function() {
  let table = '#table-extracolumns_columns';
  let form = '#form-extracolumns_columns';
  $(table + ' td.pointer').each(function() {
    $(this).removeAttr('onclick');
    $(this).removeClass('pointer');
  });
  $(table + ' td.ec-titles').each(function() {
    $(this)[0].innerHTML = "<input type='text' class='ec-input-text' value=\"" + $(this)[0].innerText + "\">"
  });
  $(table + ' td.ec-types').each(function() {
    let select = "<select class='ec-select-type'>"
        select += "<option value=''></option>"
        {foreach from=$types item=type key=value}
          if ($(this)[0].innerText == '{$value|escape:'htmlall':'UTF-8'}') {
            select += "<option value='{$value|escape:'htmlall':'UTF-8'}' selected='selected'>{$type|escape:'htmlall':'UTF-8'}</option>";
          } else {
            select += "<option value='{$value|escape:'htmlall':'UTF-8'}'>{$type|escape:'htmlall':'UTF-8'}</option>";
          }
        {/foreach}
      select += "</select>"
      $(this)[0].innerHTML = select;
  });

  $(form).on('change', '.ec-input-text', function(){
    $.ajax({
      type: 'POST',
      url: admin_extracolumns_ajax_url,
      dataType: 'json',
      data: {
        controller : 'AdminExtraColumns',
        action : 'sendInputList',
        ajax : true,
        attribute: 'title',
        EC_input : $(this).val(),
        id_extracolumns_columns : $(this).parent().parent().attr('id').split('_')[2],
      },
      success: function() {
        $.growl.notice({ title: "", message: update_success_msg });
      },
      error: function() {
        $.growl.error({ title: "", message: "An error has occurred." });
      }
    });
  });
  $(form).on('change', '.ec-select-type', function(){
    $.ajax({
      type: 'POST',
      url: admin_extracolumns_ajax_url,
      dataType: 'json',
      data: {
        controller : 'AdminExtraColumns',
        action : 'sendInputList',
        ajax : true,
        attribute: 'type',
        EC_input : $(this).val(),
        id_extracolumns_columns : $(this).parent().parent().attr('id').split('_')[2],
      },
      success: function() {
        $.growl.notice({ title: "", message: update_success_msg });
      },
      error: function() {
        $.growl.error({ title: "", message: "An error has occurred." });
      }
    });
  });
  $(form).on('click', '#desc-extracolumns_columns-save', function() {
    $('#save_template-modal').modal();
  });
  $(form).on('click', '#desc-extracolumns_columns-edit', function() {
    $('#manage_templates-modal').modal();
  });
  let profiles_dropdown = '<ul class="dropdown-menu">';
  {foreach from=$profiles item=profile}
      profiles_dropdown += '<li><a href="{$ec_link->getAdminLink('AdminExtraColumns')|escape:'htmlall':'UTF-8'}&action=load_template&id_profile={$profile.id_extracolumns_profiles|intval}">{$profile.name|escape:'htmlall':'UTF-8'}</a></li>';
  {/foreach}
  profiles_dropdown += '</ul>';

  $('#desc-extracolumns_columns-save-date').after(profiles_dropdown);
  $('#desc-extracolumns_columns-save-date').attr('data-toggle', 'dropdown');
});
</script>
